﻿namespace oracle_db_2var
{
    partial class Registration
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registration));
      this.button1 = new System.Windows.Forms.Button();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.dataSet11 = new oracle_db_2var.DataSet1();
      this.booksTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.BOOKSTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.BackColor = System.Drawing.Color.Transparent;
      this.button1.BackgroundImage = global::oracle_db_2var.Properties.Resources.plugin;
      this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
      this.button1.ForeColor = System.Drawing.Color.Transparent;
      this.button1.Location = new System.Drawing.Point(110, 184);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(64, 65);
      this.button1.TabIndex = 1;
      this.button1.UseVisualStyleBackColor = false;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // comboBox1
      // 
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(94, 130);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(96, 21);
      this.comboBox1.TabIndex = 2;
      this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(94, 158);
      this.textBox1.Name = "textBox1";
      this.textBox1.PasswordChar = '*';
      this.textBox1.Size = new System.Drawing.Size(96, 20);
      this.textBox1.TabIndex = 3;
      this.textBox1.Visible = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
      this.label1.Location = new System.Drawing.Point(44, 56);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(197, 36);
      this.label1.TabIndex = 4;
      this.label1.Text = "Библиотека v1.0";
      // 
      // dataSet11
      // 
      this.dataSet11.DataSetName = "DataSet1";
      this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // booksTableAdapter1
      // 
      this.booksTableAdapter1.ClearBeforeFill = true;
      // 
      // Registration
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
      this.ClientSize = new System.Drawing.Size(284, 261);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.textBox1);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.button1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "Registration";
      this.Text = "Form1";
      this.DoubleClick += new System.EventHandler(this.Registration_DoubleClick);
      ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    private DataSet1 dataSet11;
    private DataSet1TableAdapters.BOOKSTableAdapter booksTableAdapter1;
  }
}

