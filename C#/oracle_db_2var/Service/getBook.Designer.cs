﻿namespace oracle_db_2var
{
  partial class getBook
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
      this.button2 = new System.Windows.Forms.Button();
      this.comboBox2 = new System.Windows.Forms.ComboBox();
      this.lAB3GETCLIENTBOOKSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.cLIENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.client = new System.Windows.Forms.Label();
      this.book = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.cLIENTSTableAdapter = new oracle_db_2var.DataSet1TableAdapters.CLIENTSTableAdapter();
      this.queriesTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.QueriesTableAdapter();
      this.laB_3_GET_CLIENT_BOOKSTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.LAB_3_GET_CLIENT_BOOKSTableAdapter();
      this.booksTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.BOOKSTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.lAB3GETCLIENTBOOKSBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // dateTimePicker1
      // 
      this.dateTimePicker1.Location = new System.Drawing.Point(279, 164);
      this.dateTimePicker1.Name = "dateTimePicker1";
      this.dateTimePicker1.Size = new System.Drawing.Size(121, 20);
      this.dateTimePicker1.TabIndex = 14;
      // 
      // button2
      // 
      this.button2.BackColor = System.Drawing.Color.Transparent;
      this.button2.BackgroundImage = global::oracle_db_2var.Properties.Resources.plugin;
      this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
      this.button2.ForeColor = System.Drawing.Color.Transparent;
      this.button2.Location = new System.Drawing.Point(325, 219);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(40, 38);
      this.button2.TabIndex = 13;
      this.button2.UseVisualStyleBackColor = false;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // comboBox2
      // 
      this.comboBox2.DataSource = this.lAB3GETCLIENTBOOKSBindingSource;
      this.comboBox2.DisplayMember = "BOOK_ID";
      this.comboBox2.FormattingEnabled = true;
      this.comboBox2.Location = new System.Drawing.Point(279, 121);
      this.comboBox2.Name = "comboBox2";
      this.comboBox2.Size = new System.Drawing.Size(121, 21);
      this.comboBox2.TabIndex = 11;
      this.comboBox2.ValueMember = "BOOK_ID";
      this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
      // 
      // lAB3GETCLIENTBOOKSBindingSource
      // 
      this.lAB3GETCLIENTBOOKSBindingSource.DataMember = "LAB_3.GET_CLIENT_BOOKS";
      this.lAB3GETCLIENTBOOKSBindingSource.DataSource = this.dataSet1;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // comboBox1
      // 
      this.comboBox1.DataSource = this.cLIENTSBindingSource;
      this.comboBox1.DisplayMember = "ID";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(279, 82);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(121, 21);
      this.comboBox1.TabIndex = 12;
      this.comboBox1.ValueMember = "ID";
      this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // cLIENTSBindingSource
      // 
      this.cLIENTSBindingSource.DataMember = "CLIENTS";
      this.cLIENTSBindingSource.DataSource = this.dataSet1;
      // 
      // client
      // 
      this.client.AutoSize = true;
      this.client.BackColor = System.Drawing.Color.Transparent;
      this.client.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.client.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.client.Location = new System.Drawing.Point(427, 77);
      this.client.Name = "client";
      this.client.Size = new System.Drawing.Size(0, 25);
      this.client.TabIndex = 5;
      // 
      // book
      // 
      this.book.AutoSize = true;
      this.book.BackColor = System.Drawing.Color.Transparent;
      this.book.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.book.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.book.Location = new System.Drawing.Point(427, 116);
      this.book.Name = "book";
      this.book.Size = new System.Drawing.Size(0, 25);
      this.book.TabIndex = 6;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label4.Location = new System.Drawing.Point(139, 160);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(63, 25);
      this.label4.TabIndex = 7;
      this.label4.Text = "Дата:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label3.Location = new System.Drawing.Point(139, 116);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(67, 25);
      this.label3.TabIndex = 8;
      this.label3.Text = "Книга:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.BackColor = System.Drawing.Color.Transparent;
      this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label2.Location = new System.Drawing.Point(139, 77);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(80, 25);
      this.label2.TabIndex = 9;
      this.label2.Text = "От кого:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label1.Location = new System.Drawing.Point(283, 28);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(131, 25);
      this.label1.TabIndex = 10;
      this.label1.Text = "Возврат книги";
      // 
      // cLIENTSTableAdapter
      // 
      this.cLIENTSTableAdapter.ClearBeforeFill = true;
      // 
      // laB_3_GET_CLIENT_BOOKSTableAdapter1
      // 
      this.laB_3_GET_CLIENT_BOOKSTableAdapter1.ClearBeforeFill = true;
      // 
      // booksTableAdapter1
      // 
      this.booksTableAdapter1.ClearBeforeFill = true;
      // 
      // getBook
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(566, 284);
      this.Controls.Add(this.dateTimePicker1);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.comboBox2);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.client);
      this.Controls.Add(this.book);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "getBook";
      this.Text = "getBook";
      this.Load += new System.EventHandler(this.getBook_Load);
      ((System.ComponentModel.ISupportInitialize)(this.lAB3GETCLIENTBOOKSBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DateTimePicker dateTimePicker1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.ComboBox comboBox2;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label client;
    private System.Windows.Forms.Label book;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource cLIENTSBindingSource;
    private DataSet1TableAdapters.CLIENTSTableAdapter cLIENTSTableAdapter;
    private DataSet1TableAdapters.QueriesTableAdapter queriesTableAdapter1;
    private DataSet1TableAdapters.LAB_3_GET_CLIENT_BOOKSTableAdapter laB_3_GET_CLIENT_BOOKSTableAdapter1;
    private System.Windows.Forms.BindingSource lAB3GETCLIENTBOOKSBindingSource;
    private DataSet1TableAdapters.BOOKSTableAdapter booksTableAdapter1;
  }
}