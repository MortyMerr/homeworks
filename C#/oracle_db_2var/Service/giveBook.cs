﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class giveBook : Form
  {
    public giveBook()
    {
      InitializeComponent();
    }

    private void giveBook_Load(object sender, EventArgs e)
    {
      this.bOOKSTableAdapter.Fill(this.dataSet1.BOOKS);
      this.cLIENTSTableAdapter.Fill(this.dataSet1.CLIENTS);

    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        client.Text = queriesTableAdapter1.GET_NAME_CLIENT_BY_ID(Convert.ToDecimal(comboBox1.Text));
      }
      catch (Exception)
      {
      }
    }

    private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        book.Text = queriesTableAdapter1.GET_BOOK_NAME_BY_ID(Convert.ToDecimal(comboBox2.Text));
      }
      catch (Exception)
      {
      }
    }

    private void button2_Click(object sender, EventArgs e)
    {
      try
      {
        queriesTableAdapter1.LAB_1_INSERT_JOURNAL_RECORD(
          Convert.ToDecimal(comboBox1.Text),
          Convert.ToDecimal(comboBox2.Text),
          dateTimePicker1.Value,
          null);
        bOOKSTableAdapter.Update(dataSet1);
      }
      catch (Exception)
      {
      }
      Close();
    }
  }
}
