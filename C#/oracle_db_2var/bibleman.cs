﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class bibleman : Form
  {
    private int childFormNumber = 0;

    public bibleman()
    {
      InitializeComponent();
    }

    private void ShowNewForm(object sender, EventArgs e)
    {
      Form childForm = new Form();
      childForm.MdiParent = this;
      childForm.Text = "Окно " + childFormNumber++;
      childForm.Show();
    }

    private void OpenFile(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
      openFileDialog.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";
      if (openFileDialog.ShowDialog(this) == DialogResult.OK)
      {
        string FileName = openFileDialog.FileName;
      }
    }

    private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
      saveFileDialog.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";
      if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
      {
        string FileName = saveFileDialog.FileName;
      }
    }

    private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void CutToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }



    private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.Cascade);
    }

    private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileVertical);
    }

    private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileHorizontal);
    }

    private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.ArrangeIcons);
    }

    private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (Form childForm in MdiChildren)
      {
        childForm.Close();
      }
    }

    private void читателиToolStripMenuItem_Click(object sender, EventArgs e)
    {
      readers tmp = new readers();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void записиToolStripMenuItem_Click(object sender, EventArgs e)
    {
      journal tmp = new journal();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void книгиToolStripMenuItem_Click(object sender, EventArgs e)
    {
      books tmp = new books();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void типыКнигToolStripMenuItem_Click(object sender, EventArgs e)
    {
      bookTypes tmp = new bookTypes();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void выдачаToolStripMenuItem_Click(object sender, EventArgs e)
    {
      giveBook tmp = new giveBook();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void всеToolStripMenuItem_Click(object sender, EventArgs e)
    {
      topPopularBooks tmp = new topPopularBooks();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void уКлиентаToolStripMenuItem_Click(object sender, EventArgs e)
    {
      countClientBooks tmp = new countClientBooks();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();

    }

    private void всеToolStripMenuItem1_Click(object sender, EventArgs e)
    {
      biggestFine tmp = new biggestFine();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void уКлиентаToolStripMenuItem1_Click(object sender, EventArgs e)
    {
      clientFine tmp = new clientFine();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }

    private void приемToolStripMenuItem_Click(object sender, EventArgs e)
    {
      getBook tmp = new getBook();
      tmp.MdiParent = this;
      tmp.Text = "Окно " + childFormNumber++;
      tmp.Show();
    }
  }
}
