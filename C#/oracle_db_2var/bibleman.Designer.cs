﻿namespace oracle_db_2var
{
  partial class bibleman
  {
    /// <summary>
    /// Обязательная переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором форм Windows

    /// <summary>
    /// Требуемый метод для поддержки конструктора — не изменяйте 
    /// содержимое этого метода с помощью редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(bibleman));
      this.menuStrip = new System.Windows.Forms.MenuStrip();
      this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.журналыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.читателиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.книгиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.типыКнигToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.записиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.учетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.выдачаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.приемToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.рейтингToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.книгиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.всеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.уКлиентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.штрафыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.всеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.уКлиентаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
      this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
      this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.windowsMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.arrangeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolTip = new System.Windows.Forms.ToolTip(this.components);
      this.menuStrip.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip
      // 
      this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.toolsMenu,
            this.editMenu,
            this.windowsMenu,
            this.helpMenu});
      this.menuStrip.Location = new System.Drawing.Point(0, 0);
      this.menuStrip.MdiWindowListItem = this.windowsMenu;
      this.menuStrip.Name = "menuStrip";
      this.menuStrip.Size = new System.Drawing.Size(632, 24);
      this.menuStrip.TabIndex = 0;
      this.menuStrip.Text = "MenuStrip";
      // 
      // fileMenu
      // 
      this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.saveToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
      this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
      this.fileMenu.Name = "fileMenu";
      this.fileMenu.Size = new System.Drawing.Size(48, 20);
      this.fileMenu.Text = "&Файл";
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(169, 6);
      // 
      // saveToolStripMenuItem
      // 
      this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
      this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
      this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.saveToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
      this.saveToolStripMenuItem.Text = "&Сохранить";
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(169, 6);
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new System.Drawing.Size(169, 6);
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
      this.exitToolStripMenuItem.Text = "В&ыход";
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
      // 
      // toolsMenu
      // 
      this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.журналыToolStripMenuItem,
            this.учетToolStripMenuItem,
            this.рейтингToolStripMenuItem});
      this.toolsMenu.Name = "toolsMenu";
      this.toolsMenu.Size = new System.Drawing.Size(59, 20);
      this.toolsMenu.Text = "С&ервис";
      // 
      // журналыToolStripMenuItem
      // 
      this.журналыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.читателиToolStripMenuItem,
            this.книгиToolStripMenuItem,
            this.типыКнигToolStripMenuItem,
            this.записиToolStripMenuItem});
      this.журналыToolStripMenuItem.Image = global::oracle_db_2var.Properties.Resources.book1;
      this.журналыToolStripMenuItem.Name = "журналыToolStripMenuItem";
      this.журналыToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
      this.журналыToolStripMenuItem.Text = "Журналы";
      // 
      // читателиToolStripMenuItem
      // 
      this.читателиToolStripMenuItem.Name = "читателиToolStripMenuItem";
      this.читателиToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
      this.читателиToolStripMenuItem.Text = "Читатели";
      this.читателиToolStripMenuItem.Click += new System.EventHandler(this.читателиToolStripMenuItem_Click);
      // 
      // книгиToolStripMenuItem
      // 
      this.книгиToolStripMenuItem.Name = "книгиToolStripMenuItem";
      this.книгиToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
      this.книгиToolStripMenuItem.Text = "Книги";
      this.книгиToolStripMenuItem.Click += new System.EventHandler(this.книгиToolStripMenuItem_Click);
      // 
      // типыКнигToolStripMenuItem
      // 
      this.типыКнигToolStripMenuItem.Name = "типыКнигToolStripMenuItem";
      this.типыКнигToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
      this.типыКнигToolStripMenuItem.Text = "Типы книг";
      this.типыКнигToolStripMenuItem.Click += new System.EventHandler(this.типыКнигToolStripMenuItem_Click);
      // 
      // записиToolStripMenuItem
      // 
      this.записиToolStripMenuItem.Name = "записиToolStripMenuItem";
      this.записиToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
      this.записиToolStripMenuItem.Text = "Записи";
      this.записиToolStripMenuItem.Click += new System.EventHandler(this.записиToolStripMenuItem_Click);
      // 
      // учетToolStripMenuItem
      // 
      this.учетToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выдачаToolStripMenuItem,
            this.приемToolStripMenuItem});
      this.учетToolStripMenuItem.Image = global::oracle_db_2var.Properties.Resources.book;
      this.учетToolStripMenuItem.Name = "учетToolStripMenuItem";
      this.учетToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.учетToolStripMenuItem.Text = "Учет";
      // 
      // выдачаToolStripMenuItem
      // 
      this.выдачаToolStripMenuItem.Name = "выдачаToolStripMenuItem";
      this.выдачаToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
      this.выдачаToolStripMenuItem.Text = "Выдача";
      this.выдачаToolStripMenuItem.Click += new System.EventHandler(this.выдачаToolStripMenuItem_Click);
      // 
      // приемToolStripMenuItem
      // 
      this.приемToolStripMenuItem.Name = "приемToolStripMenuItem";
      this.приемToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
      this.приемToolStripMenuItem.Text = "Прием";
      this.приемToolStripMenuItem.Click += new System.EventHandler(this.приемToolStripMenuItem_Click);
      // 
      // рейтингToolStripMenuItem
      // 
      this.рейтингToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книгиToolStripMenuItem1,
            this.штрафыToolStripMenuItem});
      this.рейтингToolStripMenuItem.Image = global::oracle_db_2var.Properties.Resources.plugin;
      this.рейтингToolStripMenuItem.Name = "рейтингToolStripMenuItem";
      this.рейтингToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.рейтингToolStripMenuItem.Text = "Рейтинг";
      // 
      // книгиToolStripMenuItem1
      // 
      this.книгиToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.всеToolStripMenuItem,
            this.уКлиентаToolStripMenuItem});
      this.книгиToolStripMenuItem1.Name = "книгиToolStripMenuItem1";
      this.книгиToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
      this.книгиToolStripMenuItem1.Text = "Книги";
      // 
      // всеToolStripMenuItem
      // 
      this.всеToolStripMenuItem.Name = "всеToolStripMenuItem";
      this.всеToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
      this.всеToolStripMenuItem.Text = "Все";
      this.всеToolStripMenuItem.Click += new System.EventHandler(this.всеToolStripMenuItem_Click);
      // 
      // уКлиентаToolStripMenuItem
      // 
      this.уКлиентаToolStripMenuItem.Name = "уКлиентаToolStripMenuItem";
      this.уКлиентаToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
      this.уКлиентаToolStripMenuItem.Text = "У клиента";
      this.уКлиентаToolStripMenuItem.Click += new System.EventHandler(this.уКлиентаToolStripMenuItem_Click);
      // 
      // штрафыToolStripMenuItem
      // 
      this.штрафыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.всеToolStripMenuItem1,
            this.уКлиентаToolStripMenuItem1});
      this.штрафыToolStripMenuItem.Name = "штрафыToolStripMenuItem";
      this.штрафыToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
      this.штрафыToolStripMenuItem.Text = "Штрафы";
      // 
      // всеToolStripMenuItem1
      // 
      this.всеToolStripMenuItem1.Name = "всеToolStripMenuItem1";
      this.всеToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
      this.всеToolStripMenuItem1.Text = "Все";
      this.всеToolStripMenuItem1.Click += new System.EventHandler(this.всеToolStripMenuItem1_Click);
      // 
      // уКлиентаToolStripMenuItem1
      // 
      this.уКлиентаToolStripMenuItem1.Name = "уКлиентаToolStripMenuItem1";
      this.уКлиентаToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
      this.уКлиентаToolStripMenuItem1.Text = "У клиента";
      this.уКлиентаToolStripMenuItem1.Click += new System.EventHandler(this.уКлиентаToolStripMenuItem1_Click);
      // 
      // editMenu
      // 
      this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator6,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator7,
            this.selectAllToolStripMenuItem});
      this.editMenu.Name = "editMenu";
      this.editMenu.Size = new System.Drawing.Size(59, 20);
      this.editMenu.Text = "&Правка";
      // 
      // undoToolStripMenuItem
      // 
      this.undoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("undoToolStripMenuItem.Image")));
      this.undoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
      this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
      this.undoToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.undoToolStripMenuItem.Text = "&Отменить";
      // 
      // redoToolStripMenuItem
      // 
      this.redoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("redoToolStripMenuItem.Image")));
      this.redoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
      this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
      this.redoToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.redoToolStripMenuItem.Text = "&Вернуть";
      // 
      // toolStripSeparator6
      // 
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      this.toolStripSeparator6.Size = new System.Drawing.Size(187, 6);
      // 
      // cutToolStripMenuItem
      // 
      this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
      this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
      this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
      this.cutToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.cutToolStripMenuItem.Text = "&Вырезать";
      this.cutToolStripMenuItem.Click += new System.EventHandler(this.CutToolStripMenuItem_Click);
      // 
      // copyToolStripMenuItem
      // 
      this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
      this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
      this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
      this.copyToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.copyToolStripMenuItem.Text = "&Копировать";
      this.copyToolStripMenuItem.Click += new System.EventHandler(this.CopyToolStripMenuItem_Click);
      // 
      // pasteToolStripMenuItem
      // 
      this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
      this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
      this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
      this.pasteToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.pasteToolStripMenuItem.Text = "&Вставить";
      this.pasteToolStripMenuItem.Click += new System.EventHandler(this.PasteToolStripMenuItem_Click);
      // 
      // toolStripSeparator7
      // 
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      this.toolStripSeparator7.Size = new System.Drawing.Size(187, 6);
      // 
      // selectAllToolStripMenuItem
      // 
      this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
      this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
      this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
      this.selectAllToolStripMenuItem.Text = "Выделить &все";
      // 
      // windowsMenu
      // 
      this.windowsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.cascadeToolStripMenuItem,
            this.tileVerticalToolStripMenuItem,
            this.tileHorizontalToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.arrangeIconsToolStripMenuItem});
      this.windowsMenu.Name = "windowsMenu";
      this.windowsMenu.Size = new System.Drawing.Size(47, 20);
      this.windowsMenu.Text = "&Окна";
      // 
      // newWindowToolStripMenuItem
      // 
      this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
      this.newWindowToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.newWindowToolStripMenuItem.Text = "&Новое окно";
      this.newWindowToolStripMenuItem.Click += new System.EventHandler(this.ShowNewForm);
      // 
      // cascadeToolStripMenuItem
      // 
      this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
      this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.cascadeToolStripMenuItem.Text = "&Каскадом";
      this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.CascadeToolStripMenuItem_Click);
      // 
      // tileVerticalToolStripMenuItem
      // 
      this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
      this.tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.tileVerticalToolStripMenuItem.Text = "С&лева направо";
      this.tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.TileVerticalToolStripMenuItem_Click);
      // 
      // tileHorizontalToolStripMenuItem
      // 
      this.tileHorizontalToolStripMenuItem.Name = "tileHorizontalToolStripMenuItem";
      this.tileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.tileHorizontalToolStripMenuItem.Text = "С&верху вниз";
      this.tileHorizontalToolStripMenuItem.Click += new System.EventHandler(this.TileHorizontalToolStripMenuItem_Click);
      // 
      // closeAllToolStripMenuItem
      // 
      this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
      this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.closeAllToolStripMenuItem.Text = "&Закрыть все";
      this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.CloseAllToolStripMenuItem_Click);
      // 
      // arrangeIconsToolStripMenuItem
      // 
      this.arrangeIconsToolStripMenuItem.Name = "arrangeIconsToolStripMenuItem";
      this.arrangeIconsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
      this.arrangeIconsToolStripMenuItem.Text = "&Упорядочить значки";
      this.arrangeIconsToolStripMenuItem.Click += new System.EventHandler(this.ArrangeIconsToolStripMenuItem_Click);
      // 
      // helpMenu
      // 
      this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
      this.helpMenu.Name = "helpMenu";
      this.helpMenu.Size = new System.Drawing.Size(65, 20);
      this.helpMenu.Text = "&Справка";
      // 
      // contentsToolStripMenuItem
      // 
      this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
      this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
      this.contentsToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
      this.contentsToolStripMenuItem.Text = "Содер&жание";
      // 
      // indexToolStripMenuItem
      // 
      this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
      this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
      this.indexToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
      this.indexToolStripMenuItem.Text = "&Указатель";
      // 
      // searchToolStripMenuItem
      // 
      this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
      this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
      this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
      this.searchToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
      this.searchToolStripMenuItem.Text = "&Поиск";
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      this.toolStripSeparator8.Size = new System.Drawing.Size(186, 6);
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
      this.aboutToolStripMenuItem.Text = "&О программе ... ...";
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
      this.statusStrip.Location = new System.Drawing.Point(0, 431);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(632, 22);
      this.statusStrip.TabIndex = 2;
      this.statusStrip.Text = "StatusStrip";
      // 
      // toolStripStatusLabel
      // 
      this.toolStripStatusLabel.Name = "toolStripStatusLabel";
      this.toolStripStatusLabel.Size = new System.Drawing.Size(66, 17);
      this.toolStripStatusLabel.Text = "Состояние";
      // 
      // bibleman
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(632, 453);
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.menuStrip);
      this.IsMdiContainer = true;
      this.MainMenuStrip = this.menuStrip;
      this.Name = "bibleman";
      this.Text = "Библиотекарь v1.0";
      this.menuStrip.ResumeLayout(false);
      this.menuStrip.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }
    #endregion


    private System.Windows.Forms.MenuStrip menuStrip;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tileHorizontalToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fileMenu;
    private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem editMenu;
    private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolsMenu;
    private System.Windows.Forms.ToolStripMenuItem windowsMenu;
    private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem arrangeIconsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpMenu;
    private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
    private System.Windows.Forms.ToolTip toolTip;
    private System.Windows.Forms.ToolStripMenuItem журналыToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem читателиToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem книгиToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem типыКнигToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem записиToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem учетToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem выдачаToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem приемToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem рейтингToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem книгиToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem всеToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem уКлиентаToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem штрафыToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem всеToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem уКлиентаToolStripMenuItem1;
  }
}



