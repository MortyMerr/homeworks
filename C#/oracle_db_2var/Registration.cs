﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class Registration : Form
  {
    public Registration()
    {
      InitializeComponent();
      comboBox1.Items.AddRange((Information.DataBaseRole.ToArray()));
    }

    private void button1_Click(object sender, EventArgs e)
    {
      //DataSet1TableAdapters.BOOKSTableAdapter tmp2 = new DataSet1TableAdapters.BOOKSTableAdapter();
      //tmp2.Fill(dataSet11.BOOKS);
      //DataSet1.BOOKSDataTable tmp1;
      //DataSet1 tmp3 = new DataSet1();
      //dataGridView1.DataSource = booksTableAdapter1.GetData();

      // dataGridView1.Update();
      /*DataSet1.CLIENTSDataTable tmp = new DataSet1.CLIENTSDataTable();
      dataGridView1.DataSource = tmp;
      dataGridView1.Update();
      bibleman myBibleman = new bibleman();
      myBibleman.Show();*/
      try
      {
        if (comboBox1.SelectedItem.ToString() == "Библиотекарь" && textBox1.Text == Information.LazyPassword)
        {
          bibleman myBibleman = new bibleman();
          myBibleman.Show();
          Hide();
        }
        else
        {
          MessageBox.Show("Wrong passwod", "Error", MessageBoxButtons.OK);
        }
      } catch (Exception)
      {
      }
    }

    private void Registration_DoubleClick(object sender, EventArgs e)
    {
      Close();
    }

  
    private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
    {
      if (comboBox1.SelectedItem.ToString() == "Библиотекарь")
      {
        textBox1.Visible = true;
      }
    }
  }
}
