﻿namespace oracle_db_2var
{
  partial class biggestFine
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.queriesTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.QueriesTableAdapter();
      this.fine = new System.Windows.Forms.Label();
      this.client = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // fine
      // 
      this.fine.AutoSize = true;
      this.fine.BackColor = System.Drawing.Color.Transparent;
      this.fine.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.fine.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.fine.Location = new System.Drawing.Point(157, 115);
      this.fine.Name = "fine";
      this.fine.Size = new System.Drawing.Size(0, 25);
      this.fine.TabIndex = 21;
      // 
      // client
      // 
      this.client.AutoSize = true;
      this.client.BackColor = System.Drawing.Color.Transparent;
      this.client.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.client.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.client.Location = new System.Drawing.Point(298, 75);
      this.client.Name = "client";
      this.client.Size = new System.Drawing.Size(0, 25);
      this.client.TabIndex = 16;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label4.Location = new System.Drawing.Point(27, 115);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(80, 25);
      this.label4.TabIndex = 20;
      this.label4.Text = "Штраф:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label1.Location = new System.Drawing.Point(117, 50);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(205, 25);
      this.label1.TabIndex = 18;
      this.label1.Text = "Самый большой штраф:";
      // 
      // biggestFine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(440, 189);
      this.Controls.Add(this.fine);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.client);
      this.Controls.Add(this.label1);
      this.Name = "biggestFine";
      this.Text = "biggestFine";
      this.Load += new System.EventHandler(this.biggestFine_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
    private DataSet1TableAdapters.QueriesTableAdapter queriesTableAdapter1;
    private System.Windows.Forms.Label fine;
    private System.Windows.Forms.Label client;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label1;
  }
}