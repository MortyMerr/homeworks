﻿namespace oracle_db_2var
{
  partial class countClientBooks
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.client = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.queriesTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.QueriesTableAdapter();
      this.label4 = new System.Windows.Forms.Label();
      this.counter = new System.Windows.Forms.Label();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.cLIENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.cLIENTSTableAdapter = new oracle_db_2var.DataSet1TableAdapters.CLIENTSTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // comboBox1
      // 
      this.comboBox1.DataSource = this.cLIENTSBindingSource;
      this.comboBox1.DisplayMember = "ID";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(176, 101);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(121, 21);
      this.comboBox1.TabIndex = 12;
      this.comboBox1.ValueMember = "ID";
      this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // client
      // 
      this.client.AutoSize = true;
      this.client.BackColor = System.Drawing.Color.Transparent;
      this.client.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.client.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.client.Location = new System.Drawing.Point(245, 62);
      this.client.Name = "client";
      this.client.Size = new System.Drawing.Size(0, 25);
      this.client.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.BackColor = System.Drawing.Color.Transparent;
      this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label2.Location = new System.Drawing.Point(36, 96);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(80, 25);
      this.label2.TabIndex = 9;
      this.label2.Text = "Клиент:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label1.Location = new System.Drawing.Point(140, 47);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(135, 25);
      this.label1.TabIndex = 10;
      this.label1.Text = "Книги клиента";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label3.Location = new System.Drawing.Point(320, 97);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(0, 25);
      this.label3.TabIndex = 13;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label4.Location = new System.Drawing.Point(36, 154);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(151, 25);
      this.label4.TabIndex = 14;
      this.label4.Text = "Количество книг:";
      // 
      // counter
      // 
      this.counter.AutoSize = true;
      this.counter.BackColor = System.Drawing.Color.Transparent;
      this.counter.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.counter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.counter.Location = new System.Drawing.Point(193, 154);
      this.counter.Name = "counter";
      this.counter.Size = new System.Drawing.Size(0, 25);
      this.counter.TabIndex = 15;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // cLIENTSBindingSource
      // 
      this.cLIENTSBindingSource.DataMember = "CLIENTS";
      this.cLIENTSBindingSource.DataSource = this.dataSet1;
      // 
      // cLIENTSTableAdapter
      // 
      this.cLIENTSTableAdapter.ClearBeforeFill = true;
      // 
      // countClientBooks
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(392, 217);
      this.Controls.Add(this.counter);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.client);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "countClientBooks";
      this.Text = "countClientBooks";
      this.Load += new System.EventHandler(this.countClientBooks_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label client;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private DataSet1TableAdapters.QueriesTableAdapter queriesTableAdapter1;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label counter;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource cLIENTSBindingSource;
    private DataSet1TableAdapters.CLIENTSTableAdapter cLIENTSTableAdapter;
  }
}