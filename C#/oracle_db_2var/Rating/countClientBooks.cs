﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class countClientBooks : Form
  {
    public countClientBooks()
    {
      InitializeComponent();
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
         label3.Text = queriesTableAdapter1.GET_NAME_CLIENT_BY_ID(Convert.ToDecimal(comboBox1.Text));
         counter.Text = queriesTableAdapter1.LAB_1_COUNT_CLIENT_BOOKS(Convert.ToDecimal(comboBox1.Text)).ToString();
      }
      catch (Exception)
      {
      }
    }
    private void countClientBooks_Load(object sender, EventArgs e)
    {
      this.cLIENTSTableAdapter.Fill(this.dataSet1.CLIENTS);
    }
  }
}
