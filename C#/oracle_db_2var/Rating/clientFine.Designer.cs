﻿namespace oracle_db_2var
{
  partial class clientFine
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.fine = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.client = new System.Windows.Forms.Label();
      this.queriesTableAdapter1 = new oracle_db_2var.DataSet1TableAdapters.QueriesTableAdapter();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.cLIENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.cLIENTSTableAdapter = new oracle_db_2var.DataSet1TableAdapters.CLIENTSTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // fine
      // 
      this.fine.AutoSize = true;
      this.fine.BackColor = System.Drawing.Color.Transparent;
      this.fine.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.fine.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.fine.Location = new System.Drawing.Point(278, 171);
      this.fine.Name = "fine";
      this.fine.Size = new System.Drawing.Size(0, 25);
      this.fine.TabIndex = 21;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label4.Location = new System.Drawing.Point(121, 171);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(80, 25);
      this.label4.TabIndex = 20;
      this.label4.Text = "Штраф:";
      // 
      // comboBox1
      // 
      this.comboBox1.DataSource = this.cLIENTSBindingSource;
      this.comboBox1.DisplayMember = "ID";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(261, 118);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(121, 21);
      this.comboBox1.TabIndex = 19;
      this.comboBox1.ValueMember = "ID";
      this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.BackColor = System.Drawing.Color.Transparent;
      this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label2.Location = new System.Drawing.Point(121, 113);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(80, 25);
      this.label2.TabIndex = 17;
      this.label2.Text = "Клиент:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.label1.Location = new System.Drawing.Point(178, 64);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(147, 25);
      this.label1.TabIndex = 18;
      this.label1.Text = "Штраф клиента";
      // 
      // client
      // 
      this.client.AutoSize = true;
      this.client.BackColor = System.Drawing.Color.Transparent;
      this.client.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.client.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.client.Location = new System.Drawing.Point(430, 113);
      this.client.Name = "client";
      this.client.Size = new System.Drawing.Size(0, 25);
      this.client.TabIndex = 22;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // cLIENTSBindingSource
      // 
      this.cLIENTSBindingSource.DataMember = "CLIENTS";
      this.cLIENTSBindingSource.DataSource = this.dataSet1;
      // 
      // cLIENTSTableAdapter
      // 
      this.cLIENTSTableAdapter.ClearBeforeFill = true;
      // 
      // clientFine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(502, 260);
      this.Controls.Add(this.client);
      this.Controls.Add(this.fine);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "clientFine";
      this.Text = "clientFine";
      this.Load += new System.EventHandler(this.clientFine_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label fine;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label client;
    private DataSet1TableAdapters.QueriesTableAdapter queriesTableAdapter1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource cLIENTSBindingSource;
    private DataSet1TableAdapters.CLIENTSTableAdapter cLIENTSTableAdapter;
  }
}