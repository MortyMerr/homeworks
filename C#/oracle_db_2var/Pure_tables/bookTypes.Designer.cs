﻿namespace oracle_db_2var
{
  partial class bookTypes
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.bOOKTYPESBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.bOOK_TYPESTableAdapter = new oracle_db_2var.DataSet1TableAdapters.BOOK_TYPESTableAdapter();
      this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dAYCOUNTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.nAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.cNTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.fINEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.bOOKTYPESBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AutoGenerateColumns = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.dAYCOUNTDataGridViewTextBoxColumn,
            this.nAMEDataGridViewTextBoxColumn,
            this.cNTDataGridViewTextBoxColumn,
            this.fINEDataGridViewTextBoxColumn});
      this.dataGridView1.DataSource = this.bOOKTYPESBindingSource;
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(439, 260);
      this.dataGridView1.TabIndex = 0;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // bOOKTYPESBindingSource
      // 
      this.bOOKTYPESBindingSource.DataMember = "BOOK_TYPES";
      this.bOOKTYPESBindingSource.DataSource = this.dataSet1;
      // 
      // bOOK_TYPESTableAdapter
      // 
      this.bOOK_TYPESTableAdapter.ClearBeforeFill = true;
      // 
      // iDDataGridViewTextBoxColumn
      // 
      this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
      this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
      this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
      // 
      // dAYCOUNTDataGridViewTextBoxColumn
      // 
      this.dAYCOUNTDataGridViewTextBoxColumn.DataPropertyName = "DAY_COUNT";
      this.dAYCOUNTDataGridViewTextBoxColumn.HeaderText = "DAY_COUNT";
      this.dAYCOUNTDataGridViewTextBoxColumn.Name = "dAYCOUNTDataGridViewTextBoxColumn";
      // 
      // nAMEDataGridViewTextBoxColumn
      // 
      this.nAMEDataGridViewTextBoxColumn.DataPropertyName = "NAME";
      this.nAMEDataGridViewTextBoxColumn.HeaderText = "NAME";
      this.nAMEDataGridViewTextBoxColumn.Name = "nAMEDataGridViewTextBoxColumn";
      // 
      // cNTDataGridViewTextBoxColumn
      // 
      this.cNTDataGridViewTextBoxColumn.DataPropertyName = "CNT";
      this.cNTDataGridViewTextBoxColumn.HeaderText = "CNT";
      this.cNTDataGridViewTextBoxColumn.Name = "cNTDataGridViewTextBoxColumn";
      // 
      // fINEDataGridViewTextBoxColumn
      // 
      this.fINEDataGridViewTextBoxColumn.DataPropertyName = "FINE";
      this.fINEDataGridViewTextBoxColumn.HeaderText = "FINE";
      this.fINEDataGridViewTextBoxColumn.Name = "fINEDataGridViewTextBoxColumn";
      // 
      // bookTypes
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(439, 260);
      this.Controls.Add(this.dataGridView1);
      this.Name = "bookTypes";
      this.Text = "bookTypes";
      this.Load += new System.EventHandler(this.bookTypes_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.bOOKTYPESBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridView1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource bOOKTYPESBindingSource;
    private DataSet1TableAdapters.BOOK_TYPESTableAdapter bOOK_TYPESTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn dAYCOUNTDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn nAMEDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn cNTDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn fINEDataGridViewTextBoxColumn;
  }
}