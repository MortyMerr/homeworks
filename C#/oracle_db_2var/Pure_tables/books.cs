﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class books : Form
  {
    public books()
    {
      InitializeComponent();
    }

    private void books_Load(object sender, EventArgs e)
    {
      this.bOOKSTableAdapter.Fill(this.dataSet1.BOOKS);
    }
  }
}
