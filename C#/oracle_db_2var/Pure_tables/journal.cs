﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class journal : Form
  {
    public journal()
    {
      InitializeComponent();
    }

    private void journal_Load(object sender, EventArgs e)
    {
      this.jOURNALTableAdapter.Fill(this.dataSet1.JOURNAL);
    }
  }
}
