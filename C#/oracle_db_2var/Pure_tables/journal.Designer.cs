﻿namespace oracle_db_2var
{
  partial class journal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.jOURNALBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.jOURNALTableAdapter = new oracle_db_2var.DataSet1TableAdapters.JOURNALTableAdapter();
      this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.bOOKIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.cLIENTIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dATEBEGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dATERETDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.jOURNALBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AutoGenerateColumns = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.bOOKIDDataGridViewTextBoxColumn,
            this.cLIENTIDDataGridViewTextBoxColumn,
            this.dATEBEGDataGridViewTextBoxColumn,
            this.dATERETDataGridViewTextBoxColumn});
      this.dataGridView1.DataSource = this.jOURNALBindingSource;
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(430, 257);
      this.dataGridView1.TabIndex = 0;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // jOURNALBindingSource
      // 
      this.jOURNALBindingSource.DataMember = "JOURNAL";
      this.jOURNALBindingSource.DataSource = this.dataSet1;
      // 
      // jOURNALTableAdapter
      // 
      this.jOURNALTableAdapter.ClearBeforeFill = true;
      // 
      // iDDataGridViewTextBoxColumn
      // 
      this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
      this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
      this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
      this.iDDataGridViewTextBoxColumn.Width = 43;
      // 
      // bOOKIDDataGridViewTextBoxColumn
      // 
      this.bOOKIDDataGridViewTextBoxColumn.DataPropertyName = "BOOK_ID";
      this.bOOKIDDataGridViewTextBoxColumn.HeaderText = "BOOK_ID";
      this.bOOKIDDataGridViewTextBoxColumn.Name = "bOOKIDDataGridViewTextBoxColumn";
      this.bOOKIDDataGridViewTextBoxColumn.Width = 79;
      // 
      // cLIENTIDDataGridViewTextBoxColumn
      // 
      this.cLIENTIDDataGridViewTextBoxColumn.DataPropertyName = "CLIENT_ID";
      this.cLIENTIDDataGridViewTextBoxColumn.HeaderText = "CLIENT_ID";
      this.cLIENTIDDataGridViewTextBoxColumn.Name = "cLIENTIDDataGridViewTextBoxColumn";
      this.cLIENTIDDataGridViewTextBoxColumn.Width = 87;
      // 
      // dATEBEGDataGridViewTextBoxColumn
      // 
      this.dATEBEGDataGridViewTextBoxColumn.DataPropertyName = "DATE_BEG";
      this.dATEBEGDataGridViewTextBoxColumn.HeaderText = "DATE_BEG";
      this.dATEBEGDataGridViewTextBoxColumn.Name = "dATEBEGDataGridViewTextBoxColumn";
      this.dATEBEGDataGridViewTextBoxColumn.Width = 89;
      // 
      // dATERETDataGridViewTextBoxColumn
      // 
      this.dATERETDataGridViewTextBoxColumn.DataPropertyName = "DATE_RET";
      this.dATERETDataGridViewTextBoxColumn.HeaderText = "DATE_RET";
      this.dATERETDataGridViewTextBoxColumn.Name = "dATERETDataGridViewTextBoxColumn";
      this.dATERETDataGridViewTextBoxColumn.Width = 89;
      // 
      // journal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(430, 257);
      this.Controls.Add(this.dataGridView1);
      this.Name = "journal";
      this.Text = "journal";
      this.Load += new System.EventHandler(this.journal_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.jOURNALBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridView1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource jOURNALBindingSource;
    private DataSet1TableAdapters.JOURNALTableAdapter jOURNALTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn bOOKIDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn cLIENTIDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn dATEBEGDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn dATERETDataGridViewTextBoxColumn;
  }
}