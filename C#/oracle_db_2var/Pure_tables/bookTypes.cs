﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oracle_db_2var
{
  public partial class bookTypes : Form
  {
    public bookTypes()
    {
      InitializeComponent();
    }

    private void bookTypes_Load(object sender, EventArgs e)
    {
      this.bOOK_TYPESTableAdapter.Fill(this.dataSet1.BOOK_TYPES);
    }
  }
}
