﻿namespace oracle_db_2var
{
  partial class books
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(books));
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.bOOKSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.bOOKSTableAdapter = new oracle_db_2var.DataSet1TableAdapters.BOOKSTableAdapter();
      this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.nAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.cNTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tYPEIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.bOOKSBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AutoGenerateColumns = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nAMEDataGridViewTextBoxColumn,
            this.cNTDataGridViewTextBoxColumn,
            this.tYPEIDDataGridViewTextBoxColumn});
      this.dataGridView1.DataSource = this.bOOKSBindingSource;
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(484, 264);
      this.dataGridView1.TabIndex = 0;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // bOOKSBindingSource
      // 
      this.bOOKSBindingSource.DataMember = "BOOKS";
      this.bOOKSBindingSource.DataSource = this.dataSet1;
      // 
      // bOOKSTableAdapter
      // 
      this.bOOKSTableAdapter.ClearBeforeFill = true;
      // 
      // iDDataGridViewTextBoxColumn
      // 
      this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
      this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
      this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
      // 
      // nAMEDataGridViewTextBoxColumn
      // 
      this.nAMEDataGridViewTextBoxColumn.DataPropertyName = "NAME";
      this.nAMEDataGridViewTextBoxColumn.HeaderText = "NAME";
      this.nAMEDataGridViewTextBoxColumn.Name = "nAMEDataGridViewTextBoxColumn";
      // 
      // cNTDataGridViewTextBoxColumn
      // 
      this.cNTDataGridViewTextBoxColumn.DataPropertyName = "CNT";
      this.cNTDataGridViewTextBoxColumn.HeaderText = "CNT";
      this.cNTDataGridViewTextBoxColumn.Name = "cNTDataGridViewTextBoxColumn";
      // 
      // tYPEIDDataGridViewTextBoxColumn
      // 
      this.tYPEIDDataGridViewTextBoxColumn.DataPropertyName = "TYPE_ID";
      this.tYPEIDDataGridViewTextBoxColumn.HeaderText = "TYPE_ID";
      this.tYPEIDDataGridViewTextBoxColumn.Name = "tYPEIDDataGridViewTextBoxColumn";
      // 
      // books
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::oracle_db_2var.Properties.Resources.bground;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(484, 264);
      this.Controls.Add(this.dataGridView1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "books";
      this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
      this.Text = "books";
      this.Load += new System.EventHandler(this.books_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.bOOKSBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridView1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource bOOKSBindingSource;
    private DataSet1TableAdapters.BOOKSTableAdapter bOOKSTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn nAMEDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn cNTDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tYPEIDDataGridViewTextBoxColumn;
  }
}