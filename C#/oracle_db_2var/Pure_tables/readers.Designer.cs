﻿namespace oracle_db_2var
{
  partial class readers
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(readers));
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.dataSet1 = new oracle_db_2var.DataSet1();
      this.cLIENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.cLIENTSTableAdapter = new oracle_db_2var.DataSet1TableAdapters.CLIENTSTableAdapter();
      this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.lASTNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.fATHERNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.pASSPORTSERIADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.pASPORTNUMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.fIRSTNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AutoGenerateColumns = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.lASTNAMEDataGridViewTextBoxColumn,
            this.fATHERNAMEDataGridViewTextBoxColumn,
            this.pASSPORTSERIADataGridViewTextBoxColumn,
            this.pASPORTNUMDataGridViewTextBoxColumn,
            this.fIRSTNAMEDataGridViewTextBoxColumn});
      this.dataGridView1.DataSource = this.cLIENTSBindingSource;
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(536, 256);
      this.dataGridView1.TabIndex = 0;
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "DataSet1";
      this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // cLIENTSBindingSource
      // 
      this.cLIENTSBindingSource.DataMember = "CLIENTS";
      this.cLIENTSBindingSource.DataSource = this.dataSet1;
      // 
      // cLIENTSTableAdapter
      // 
      this.cLIENTSTableAdapter.ClearBeforeFill = true;
      // 
      // iDDataGridViewTextBoxColumn
      // 
      this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
      this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
      this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
      // 
      // lASTNAMEDataGridViewTextBoxColumn
      // 
      this.lASTNAMEDataGridViewTextBoxColumn.DataPropertyName = "LAST_NAME";
      this.lASTNAMEDataGridViewTextBoxColumn.HeaderText = "LAST_NAME";
      this.lASTNAMEDataGridViewTextBoxColumn.Name = "lASTNAMEDataGridViewTextBoxColumn";
      // 
      // fATHERNAMEDataGridViewTextBoxColumn
      // 
      this.fATHERNAMEDataGridViewTextBoxColumn.DataPropertyName = "FATHER_NAME";
      this.fATHERNAMEDataGridViewTextBoxColumn.HeaderText = "FATHER_NAME";
      this.fATHERNAMEDataGridViewTextBoxColumn.Name = "fATHERNAMEDataGridViewTextBoxColumn";
      // 
      // pASSPORTSERIADataGridViewTextBoxColumn
      // 
      this.pASSPORTSERIADataGridViewTextBoxColumn.DataPropertyName = "PASSPORT_SERIA";
      this.pASSPORTSERIADataGridViewTextBoxColumn.HeaderText = "PASSPORT_SERIA";
      this.pASSPORTSERIADataGridViewTextBoxColumn.Name = "pASSPORTSERIADataGridViewTextBoxColumn";
      // 
      // pASPORTNUMDataGridViewTextBoxColumn
      // 
      this.pASPORTNUMDataGridViewTextBoxColumn.DataPropertyName = "PASPORT_NUM";
      this.pASPORTNUMDataGridViewTextBoxColumn.HeaderText = "PASPORT_NUM";
      this.pASPORTNUMDataGridViewTextBoxColumn.Name = "pASPORTNUMDataGridViewTextBoxColumn";
      // 
      // fIRSTNAMEDataGridViewTextBoxColumn
      // 
      this.fIRSTNAMEDataGridViewTextBoxColumn.DataPropertyName = "FIRST_NAME";
      this.fIRSTNAMEDataGridViewTextBoxColumn.HeaderText = "FIRST_NAME";
      this.fIRSTNAMEDataGridViewTextBoxColumn.Name = "fIRSTNAMEDataGridViewTextBoxColumn";
      // 
      // readers
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new System.Drawing.Size(536, 256);
      this.Controls.Add(this.dataGridView1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "readers";
      this.Text = "readers";
      this.Load += new System.EventHandler(this.readers_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cLIENTSBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridView1;
    private DataSet1 dataSet1;
    private System.Windows.Forms.BindingSource cLIENTSBindingSource;
    private DataSet1TableAdapters.CLIENTSTableAdapter cLIENTSTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn lASTNAMEDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn fATHERNAMEDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn pASSPORTSERIADataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn pASPORTNUMDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn fIRSTNAMEDataGridViewTextBoxColumn;
  }
}