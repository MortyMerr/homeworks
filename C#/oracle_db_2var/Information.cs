﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oracle_db_2var
{
  class Information
  {
    private static List<String> dataBaseRole = new List<String> { "Читатель", "Библиотекарь" };
    public static List<String> DataBaseRole
    {
      get
      {
        return dataBaseRole;
      }
    }

    //TODO possibly add password query from database
    private static string lazyPassword = "0000";
    public static string LazyPassword
    {
      get
      {
        return lazyPassword;
      }
    }
  }
}
