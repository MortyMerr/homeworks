package RunLengthEncode;

/**
 * Created by adm on 14.04.2017.
 */
public class FileDecoder extends FileHandle {
    @Override
    protected void handle() {/*распковка
    бежим по строке, если встречаем символ числа - начинаем прибавлять символы к строке
    Это сделано затем, чтобы был корректно прочитано
    А119 (119 повторений буквы а)
    Далее записываем символ столько раз, сколько указано в счетчике*/
        StringBuilder tmp = new StringBuilder("");
        int i = 0;
        while (i < inf.length) {
            if (inf[i] >= '0' && inf[i] <= '9') {
                char symbol = inf[i - 1];
                StringBuilder num = new StringBuilder(String.valueOf(inf[i]));
                while (inf[++i] >= '0' && inf[i] <= '9') {
                    num.append(inf[i]);
                }
                for (int j = 1; j < Integer.parseInt(num.toString()); j++) {
                    tmp.append(symbol);
                }
            } else {
                tmp.append(inf[i++]);
            }
        }
        answer = tmp.toString();
    }
    public String genOutName(String input){
        return input.substring(0, input.length() - 4).concat(".UNP");
    }
}
