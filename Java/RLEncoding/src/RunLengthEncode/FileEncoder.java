package RunLengthEncode;

/**
 * Created by adm on 13.04.2017.
 */
public class FileEncoder extends FileHandle {
    @Override
    protected void handle() {
        /*алгоритм такой начинаем с нулевого элемента строки, запомнили его
        дальше начинаем прроверять -если следующий элемент повторяет наш запомненый
        то увелличиваем счетчик. Если нет(новая буква) то записываем наш запомненный элемент
        и количество его повторений (если оно не равно 1)
        Непонято объяснил, но ты посмотри код и спроси если что
         */
        char comp = inf[0];
        StringBuilder tmp = new StringBuilder("");
        int counter = 1;
        for (int i = 1; i < inf.length; i++) {
            if (inf[i] != comp) {
                tmp.append(comp);
                comp = inf[i];
                if (counter != 1) {
                    tmp.append(counter);
                }
                counter = 0;
            }
            counter++;
        }
        tmp.append(comp);
        if (counter != 1) {//TODO fix repeated code
            tmp.append(counter);
        }
        answer = tmp.toString();
    }
    @Override
    public String genOutName(String input){
        return input.substring(0, input.length() - 4).concat(".RLE");
    }
}
