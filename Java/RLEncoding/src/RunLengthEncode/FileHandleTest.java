package RunLengthEncode;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by adm on 14.04.2017.
 */
public class FileHandleTest extends TestCase {
//    тесты. Тут все просто. Создаем файл, пишем в него строку, потом сжимаем строку
//    и сравниваем ее с выражением, которое должно получиться(мы посчитали сами) и в обртную сторону
    private FileHandle fileHandle;
    private final String in = "test\\test.txt",
            testString = "aaaaaBBBcccDDDEE",
            testResult = "a5B3c3D3E2";

    @Before
    protected void setUpTestFile() {
        try (FileWriter fileWriter = new FileWriter(new File(in))) {
            fileWriter.write(testString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEncode() {
        fileHandle = new FileEncoder();
        fileHandle.pack(in, fileHandle.genOutName(in));
        try (Scanner input = new Scanner(new File(fileHandle.genOutName(in)))) {
            String answer = input.nextLine();
            assertEquals(testResult, answer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDecode() {
        fileHandle = new FileDecoder();
        fileHandle.pack(in, fileHandle.genOutName(in));
        try (Scanner input = new Scanner(new File(fileHandle.genOutName(in)))) {
            String answer = input.nextLine();
            assertEquals(testString, answer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
