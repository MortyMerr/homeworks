package RunLengthEncode;

/**
 * Created by adm on 13.04.2017.
 */
public class CommandHandler {
    public static void main(String[] args) {
//        !!!!!!!!!!!!!!!
//        коммментарии читать в следующем порядке - FileHandle, FIleEncoder,FileDecoder, Этот файл, FileHandleTest
//        !!!!!!!!!!!!!!!

//        args = new String[]{"-z", "txt\\input.txt"};
//        args = new String[]{"-u", "txt\\input.txt.RLE"};
//        args = new String[]{"-z", "-out", "txt\\out2.txt", "txt\\biginput.txt"};
//        Если очень хочется запускать не из командной строки, а тыкать на зеленую стрелочку, то раскоменти одну
//                из этих строк (думаю, понятно где какая команда)
//        Как запускать из командной строки?
//        в папке out/artifacts лежит собранный исполняемый файл. Нужно из командной строки перейти в его директорию
//        и написать следующее
//        java -jar RLEncoding.jar -z -out C:\Users\adm\IdeaProjects\RLEncoding\txt\answer.txt C:\Users\adm\IdeaProjects\RLEncoding\txt\input.txt
//        Это пример с моим полным путем. У тебя он другой. Если использовать просто назвния типа input.txt output.txt то
//         файлы будут размещены в out/artifacts
        if (args.length == 4 || args.length == 2) {//Обработка консольноый команды
            FileHandle fileHandle;
            if (args[0].equals("-z")) {
                fileHandle = new FileEncoder();
            } else if (args[0].equals("-u")) {
                fileHandle = new FileDecoder();
            } else {
                throw new IllegalArgumentException("Bad key");
            }
            if (args[1].equals("-out")) {
                fileHandle.pack(args[3], args[2]);
            } else {
                System.out.println(args[1]);
                fileHandle.pack(args[1], fileHandle.genOutName(args[1]));
            }
        }
    }
}
