package RunLengthEncode;

import java.io.*;

/**
 * Created by adm on 13.04.2017.
 */
/*
Это абстрактный класс. В нем определены методы чтения (Потому что и для шифровщика и для дешифровщика они одинаковые
а метод handle() - обработка для каждого разный.
 */
public abstract class FileHandle {
    char[] inf;//сюда считаем входной файл
    String answer;//сюда запишем ответ

    public void pack(String input, String output) {
        File in = new File(input);//открыли файл
        if (in.exists()) {//если сущществует сделали
            read(in);//вызвали метод
            handle();//обработали
            File out = new File(output);//открыли выходной
            write(out);//вызвали метод
        } else {
            System.out.println("Input file not found");//если не нашли вывели ошибку
        }
    }

    protected boolean read(File in) {
        try (FileReader input = new FileReader(in)) {//чтеник
            long length = in.length();
            inf = new char[(int) length];
            input.read(inf);
            System.out.println(inf[0]);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected boolean write(File out) {//запись
        try (FileWriter output = new FileWriter(out)) {
            output.write(answer);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    abstract public String genOutName(String input);

    abstract protected void handle();
}
