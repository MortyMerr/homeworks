package ru.spbpu.ics.main;

import ru.spbpu.ics.gui.form.MainFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        new MainFrame();
                    }
                });
    }
}
        /*ApplicationContext context2 = new AnnotationConfigApplicationContext(SqlConfig.class);
        PersonSql sqLite = (PersonSql) context2.getBean("personSQL");
		MainFrame().
		//		Person p = new Person();
//		p.setFirstName("Alexey");
//		p.setLastName("Falko");
//		p.setAge(22);
		
//		Person p1 = new Person();
//		p1.setFirstName("Lewis");
//		p1.setLastName("Hamilton");
//		p1.setAge(31);
		sqLite.drop();
		sqLite.create();
//		sqLite.insertWithJDBC(p);
		
//		sqLite.updateByID(4, p1);

		Consumer<Person> consumer = p -> System.out.println(p.getFirstName()+ " | " 
									+ p.getLastName());

		sqLite.getAll().forEach(consumer);*/
