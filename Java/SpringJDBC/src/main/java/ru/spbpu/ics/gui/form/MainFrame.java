package ru.spbpu.ics.gui.form;

import ru.spbpu.ics.gui.table_model.PersonModel;

import javax.swing.*;

/**
 * Created by adm on 17.04.2017.
 */
public class MainFrame extends JFrame{
    private JTable table1;
    private JPanel MainPanel;
    private JButton button1;

    public MainFrame() {
        super();
        table1.setModel(new PersonModel());
        setContentPane(MainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
