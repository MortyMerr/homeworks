package ru.spbpu.ics.dao.logic;

import java.sql.Date;

public class Person {
    private int id;
    private String firstName, lastName, district;
    private Date birth;

    public Object[] getFieldsOnjects() {
        return new Object[]{id, firstName, lastName, district, birth};
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }
}
