package ru.spbpu.ics.dao.interfaces;

import ru.spbpu.ics.dao.logic.Person;

import java.util.List;

public interface PersonDao{
	public void insert(Person person);
	List<Person> getAll();
	public void updateByID(int id, Person person);
	public void deleteByID(int id);
	Person getByID(int id);
	public void deleteAll();
}
