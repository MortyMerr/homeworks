#ifndef SHEDULECREATOR_H
#define SHEDULECREATOR_H
#include "Shedule.h"
#include <QFile>
#include <QString>
#include <QVector>
#include <QTextStream>
#include <QException>
#include <QDebug>

class SheduleParser final : public Shedule
{
public:
  SheduleParser(const QString& file);
  QString* getTrainList() const;
  void sortByNum();
  QVector <train_t> const & getTrains() const;
private:
  QString* trainList_;
  QFile file_;
  void parseFile();
};

#endif // SHEDULECREATOR_H
