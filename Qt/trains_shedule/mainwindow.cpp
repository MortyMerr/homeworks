#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  stations = 5;
  ui->setupUi(this);
  /*SheduleGenerator temp(stations, 5);
  temp.toFile("shedule.txt");*/
  //нужно для генерации собственного файла с расписанием поездов
  ui->textBrowser->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  ui->textBrowser->setAlignment(Qt::AlignBottom | Qt::AlignRight);
}

MainWindow::~MainWindow()
{
  delete ui;
  if(shedule != nullptr)
  {
    delete shedule;
  }
}

void MainWindow::on_openFile_clicked()
{
  const QString filename = QFileDialog::getOpenFileName(this,tr("Open shedule"),
                                                        QDir::currentPath(),
                                                        tr("txt Files (*.txt)"));
  shedule = new SheduleParser(filename);
  updateBrowser(*shedule->getTrainList());
}

void MainWindow::on_saveFile_clicked()
{
  const QString filename = QFileDialog::getSaveFileName(this, tr("Save shedule"),
                                                        QDir::currentPath(),
                                                        tr("txt files (*.txt)"));
  QFile f(filename);
  if(f.open(QIODevice::WriteOnly))
  {
    QTextStream fout(&f);
    fout << ui->textBrowser->toPlainText();
    f.close();
  }
}

void MainWindow::on_sortBuNum_clicked()
{
  shedule->sortByNum();
  updateBrowser(*shedule->getTrainList());
}

void MainWindow::updateBrowser(const QString& out)
{
  ui->textBrowser->clear();
  ui->textBrowser->append(out);
}

void MainWindow::on_findWay_clicked()
{
  if(ui->a->text().length() != 0 && ui->b->text().length() != 0 && shedule != nullptr)
  {
    try
    {
      bool ok;
      GrafMakeSolv solver(shedule->getTrains(), stations);
      solver.findWays(ui->a->text().toUInt(&ok), ui->b->text().toUInt(&ok));
      updateBrowser(solver.getWays());
    }
    catch(QException e)
    {
      qDebug() << "Incorrect format" << e.what();
    }
  }
}

void MainWindow::on_readme_clicked()
{
  ui->textBrowser->clear();
  ui->textBrowser->append("Открыть файл - загрузить расписание поездов из проводник.\n\n"
                          "Сохранить в файл - сохранить информацию из текстового окна в файл.\n\n"
                          "Отсортировать по номеру - отсортировать поезда по номеру.\n\n"
                          "Найти пути - найти все пути из точки в точку Б. Перед этим необходимо загрузить"
                          "расписание. Выходная информацию следует понимать так <номер поезда>:<номер станции>."
                          "То есть первая цифра означает номер поезда, на котором предложено ехать до этой станции."
                          "0 - означает начальную станцию.\n\n"
                          "Минимум пересадок - самый короткий путь по количеству станций и пересадок.\n\n");
}

void MainWindow::on_shortestWays_clicked()
{
  //TODO reusing code
  if(ui->a->text().length() != 0 && ui->b->text().length() != 0 && shedule != nullptr)
  {
    try
    {
      bool ok;
      GrafMakeSolv solver(shedule->getTrains(), stations);
      solver.findSWays(ui->a->text().toUInt(&ok), ui->b->text().toUInt(&ok));
      updateBrowser(solver.getWays());
    }
    catch(QException e)
    {
      qDebug() << "Incorrect format" << e.what();
    }
  }
}
