#include "GrafMakeSolv.h"
GrafMakeSolv::GrafMakeSolv(const QVector <train_t>& inf, const size_t stations)
{
  QVector <size_t> tmp2;
  QVector <QVector <size_t> > tmp;
  for(size_t i = 0; i <= stations; i++)
  {
    tmp.push_back(tmp2);
  }
  for(size_t i = 0; i <= stations; i++)
  {
    graf_.push_back(tmp);
  }
  fillGraph(inf);
}

QString GrafMakeSolv::getWays()
{
  QString tmp = "";
  std::for_each(shortestWays_.begin(), shortestWays_.end(), [&tmp] (const way_t& r) mutable
  {
    std::for_each(r.stations.begin(), r.stations.end(), [&tmp] (const station_t& st) mutable
    {
      return tmp += QString::number(st.trainNum) + ':' + QString::number(st.stationNum) + " ";
    });
    return tmp += "Пересадок: " + QString::number(r.transferCount) + '\n';
  });
  return tmp;
}

void GrafMakeSolv::fillGraph(const QVector<train_t>&inf)
{
  for(size_t i = 0; i < (size_t)inf.size(); i++)
  {
    for(size_t j = 1; j < (size_t)inf[i].stations.size(); j++)
    {
      graf_[inf[i].stations[j]][inf[i].stations[j-1]].push_back(inf[i].number);
      graf_[inf[i].stations[j-1]][inf[i].stations[j]].push_back(inf[i].number);
    }
  }
}

void GrafMakeSolv::findWays(const size_t a, const size_t b)
{
  shortestWays_.clear();
  QVector <way_t> tmpWays;
  tmpWays.push_back({{{a, 0}}, 0});
  //иницализация
  while(!tmpWays.empty())
  {
    const way_t myWay = *tmpWays.begin();
    if(myWay.stations.size() != 1 && myWay.stations.last().stationNum == b)
    {
      shortestWays_.push_back(myWay);
      tmpWays.pop_front();
      continue;
    }
    for(size_t i = 0; i < (size_t)graf_[myWay.stations.last().stationNum].size(); i++)
    {
      for(size_t j = 0; j < (size_t)graf_[myWay.stations.last().stationNum][i].size(); j++)
        //по всем вершинам в которые можно поехать из последней вершины рассматриваемого пути
      {
        if(std::find_if(myWay.stations.begin(), myWay.stations.end(),
                        [i] (const station_t& s)
        { return (s.stationNum == i);}) == myWay.stations.end())
        {
          tmpWays.push_back(myWay);
          if(tmpWays.last().stations.last().trainNum != graf_[myWay.stations.last().stationNum][i][j])
          {
            tmpWays.last().transferCount++;
          }
          tmpWays.last().stations.push_back({i, graf_[myWay.stations.last().stationNum][i][j]});
        }
      }
    }
    tmpWays.pop_front();
  }
}

void GrafMakeSolv::findSWays(const size_t a, const size_t b)
{
  findWays(a, b);
  std::sort(shortestWays_.begin(), shortestWays_.end(), [] (const way_t& l, const way_t& r)
  {
    return (l.stations.size() < r.stations.size());
  });
  shortestWays_.erase(shortestWays_.begin() + 1, shortestWays_.end());
}
