#ifndef SHEDULE_H
#define SHEDULE_H
#include <QVector>
#include <QString>

struct train_t{
  size_t number;
  QVector <size_t> stations;
};

class Shedule
{
protected:
  QVector <train_t > trains_;
  QString* getInf() const;
};

#endif // SHEDULE_H
