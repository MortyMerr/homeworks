#ifndef GRAFMAKESOLV_H
#define GRAFMAKESOLV_H
#include "Shedule.h"
#include <QVector>


class GrafMakeSolv
{
public:
  struct station_t{
    size_t stationNum, trainNum;
  };
  struct way_t{
    QVector <station_t> stations;
    size_t transferCount;
  };
  GrafMakeSolv(const QVector <train_t>& inf, const size_t stations);
  QString getWays();
  void findWays(const size_t a, const size_t b);
  void findSWays(const size_t a, const size_t b);
private:
  QVector <way_t> shortestWays_;
  QVector <QVector <QVector <size_t> > > graf_;
  void fillGraph(const QVector <train_t>& inf);
};

#endif // GRAFMAKESOLV_H
