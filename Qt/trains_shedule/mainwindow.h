#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "SheduleGenerator.h"
#include "SheduleParser.h"
#include "GrafMakeSolv.h"
#include <QMainWindow>
#include <QFileDialog>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
  void on_openFile_clicked();

  void on_saveFile_clicked();

  void on_sortBuNum_clicked();

  void on_findWay_clicked();

  void on_readme_clicked();

  void on_shortestWays_clicked();

private:
    Ui::MainWindow *ui;
private:
    SheduleParser* shedule;
    size_t stations;
    void updateBrowser(const QString& out);
};

#endif // MAINWINDOW_H
