#include "Shedule.h"

QString* Shedule::getInf() const
{
  QString* tmp = new QString("");
  for(const train_t& t : trains_)
  {
    *tmp += QString::number(t.number) + " : ";
    for(auto s = t.stations.begin(); s < t.stations.end() - 1; s++)
    {
      *tmp += QString::number(*s) + "-";
    }
    *tmp += QString::number(*(t.stations.end() - 1)) + '\n';
  }
  return tmp;
}
