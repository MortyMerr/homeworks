#include "SheduleParser.h"

SheduleParser::SheduleParser(const QString &file)
{
  if(QFile::exists(file))
  {
    qDebug() << "File exist";
    file_.setFileName(file);
    parseFile();
  }
}

void SheduleParser::parseFile()
{
  if(file_.open(QIODevice::ReadOnly))
  {
    qDebug() << "File opened";
    QTextStream tmp(&file_);
    while(!tmp.atEnd())
    {
      try
      {
        //TODO regexp for check
        size_t num = 0;
        QString stations = "";

        //TODO fix
        tmp >> num >> stations;
        stations = tmp.readLine();
        QVector<QString> temp = stations.split("-").toVector();
        QVector<size_t> temp2(temp.length());
        bool ok;
        for(size_t i = 0; i < (size_t)temp.length(); i++)
        {
          temp2[i] = temp[i].toUInt(&ok);
        }
        //

        trains_.push_back(train_t{num, temp2});
      }
      catch(QException e)
      {
        qDebug() << e.what();
      }
    }
    file_.close();
    qDebug() << "File closed";
    trainList_ = getInf();
  }
}

QString* SheduleParser::getTrainList() const
{
  return trainList_;
}

void SheduleParser::sortByNum()
{
  std::sort(trains_.begin(), trains_.end(), [] (const train_t& l, const train_t& r)
  {return l.number < r.number;});
  trainList_ = getInf();
}

 QVector<train_t> const & SheduleParser::getTrains() const
 {
   return trains_;
 }
