#-------------------------------------------------
#
# Project created by QtCreator 2017-04-03T18:19:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = trainspotting
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    SheduleGenerator.cpp \
    Shedule.cpp \
    SheduleParser.cpp \
    GrafMakeSolv.cpp

HEADERS  += mainwindow.h \
    SheduleGenerator.h \
    Shedule.h \
    SheduleParser.h \
    GrafMakeSolv.h

FORMS    += mainwindow.ui
