#ifndef SHEDULEGENERATOR_H
#define SHEDULEGENERATOR_H
#include "Shedule.h"
#include <QString>
#include <QTime>
#include <algorithm>
#include <QFile>
#include <QTextStream>

class SheduleGenerator final : public Shedule
{
public:
  SheduleGenerator(const size_t stations, const size_t trains);
  void generateShedule(const size_t stations, const size_t trains);
  void toFile(const QString& file) const;
};

#endif // SHEDULEGENERATOR_H
