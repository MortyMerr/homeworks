#include "SheduleGenerator.h"

SheduleGenerator::SheduleGenerator(const size_t stations, const size_t trains)
{
  QTime midnight(0,0,0);
  qsrand(midnight.secsTo(QTime::currentTime()));
  generateShedule(stations, trains);
}

void SheduleGenerator::generateShedule(const size_t stations, const size_t trains)
{
  const size_t minLength = 2;
  size_t count = 1;
  QVector <size_t> allNumbers(trains);
  std::generate(allNumbers.begin(), allNumbers.end(), [] () {return qrand()%1000; });
  QVector <size_t> allStations(stations);
  std::generate(allStations.begin(), allStations.end(), [&count] () { return count++;});
  for(size_t i = 0; i < trains; i++)
  {
    train_t tmp;
    tmp.number = allNumbers[i];
    std::random_shuffle(allStations.begin(), allStations.end());
    tmp.stations = allStations;
    tmp.stations.erase(tmp.stations.begin() + qrand()%(stations - minLength) + minLength, tmp.stations.end());
    trains_.push_back(tmp);
  }
}

void SheduleGenerator::toFile(const QString& file) const
{
  QFile f(file);
  if(f.open(QIODevice::WriteOnly))
  {
    QTextStream fout(&f);
    fout << *getInf();
    f.close();
  }
}

