#ifndef RESOURCE_HPP
#define RESOURCE_HPP

#include <string>

class Resource
{
public:
  std::string name_;
  Resource(std::string name);
  bool entry();
  bool release();
  friend std::ostream & operator <<(std::ostream& os, const Resource tmp);
private:
  bool locked_;
};


#endif // !RESOURCE_HPP
