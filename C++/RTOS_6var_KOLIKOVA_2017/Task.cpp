#include "Task.hpp"
#include <algorithm>
#include "logger.hpp"

Task::Task(double born,
  double execTime,
  double period,
  const std::initializer_list< Resource* >& resources,
  const missionFunction mission,
  const std::string& name,
  const std::initializer_list<Task *>&& taskToCall,
  int needSignal,
  size_t priority) :
  born_(born),
  execTime_(execTime),
  period_(period),
  resources_(resources),
  mission_(mission),
  name_(name),
  taskToCall_(taskToCall),
  needSighnal_(needSignal),
  priority_(priority)
{
  name_ = getDescription();
  log("_________created " + name_);
}

int Task::run() {
  if (needSighnal_ != -1 && !events[needSighnal_]) {
    log("no need signal, sleep " + name_);
    return 1;
  }
  log("Running " + name_);
  while (!resources_.empty())
  {
    log("try to catch " + resources_.front()->name_);
    if (!resources_.front()->entry()) {
      log("resource " + resources_.front()->name_ + " busy, go to sleep " + name_);
      return 1;//sleep
    } else {
      log("catched " + resources_.front()->name_ + " by " + name_);
    }
    log("calling mission func by " + name_);
    mission_(taskToCall_);
    if (!resources_.front()->release()) {
      log("trying to release released " + name_ + ' ' + resources_.front()->name_);
      throw std::invalid_argument("trying to release released " + name_ + ' ' + resources_.front()->name_);
    } else {
      log("released " + resources_.front()->name_ + " by " + name_);
    }
    resources_.erase(resources_.begin());
    return 2;//wanna update shedule
  }
  return 0;
}

Task::~Task()
{
  log("_________destroyed " + name_);
}

std::string Task::getDescription() const
{
  char bufer[10];
  _itoa_s(priority_, bufer, 10);
  return name_ + " priority " + bufer;
}
