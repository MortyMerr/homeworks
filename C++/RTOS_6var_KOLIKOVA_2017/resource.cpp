#include "resource.hpp"
#include "utils.hpp"
#include <iostream>
#include <stdexcept>

Resource::Resource(std::string name) :
  name_(name),
  locked_(false)
{
  log("Resource created" + name);
}


bool Resource::entry()
{
  if (locked_) {
    return false;
  }
  locked_ = true;
}

bool Resource::release()
{
  if (!locked_) {
    return false;
  }
  locked_ = false;
}