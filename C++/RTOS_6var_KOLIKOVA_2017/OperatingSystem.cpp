#include "OperatingSystem.hpp"
#include <deque>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <iostream>

OperatingSystem& OperatingSystem::Instance()
{
  static OperatingSystem s;
  return s;
}

OperatingSystem::OperatingSystem()
{
  log("Operating system started");
  std::vector< Task* > tmp;
  shedule_.resize(maxTask);
}

OperatingSystem::~OperatingSystem()
{
  log("Operating system shutted down");
}

void OperatingSystem::push(Task* task)
{
  log("Sheduling task " + task->name_);
  if (task->priority_ != 0) {
    shedule_[task->priority_].push_back(task);
    return;
  }
}

void OperatingSystem::start()
{
  Task * tmp = nullptr;
  updateShedule();
  while (!finalShedule_.empty()) {
    log("trying to run " + (*finalShedule_.begin())->name_);
    switch ((*finalShedule_.begin())->run()) {
    case 0:
      delete *finalShedule_.begin();
      finalShedule_.pop_front();
      break;
    case 1:
      tmp = *finalShedule_.begin();
      finalShedule_.pop_front();
      finalShedule_.insert(finalShedule_.begin() + 1, tmp);
      break;
    case 2:
      delete *finalShedule_.begin();
      finalShedule_.pop_front();
      updateShedule();
      break;
    }
  }
}

void OperatingSystem::updateShedule()
{
  std::for_each(std::cbegin(shedule_), std::cend(shedule_), [this](const auto & vector) mutable
  {
    std::for_each(std::cbegin(vector), std::cend(vector), [this](Task* task) mutable
    {
      this->pasteInDeque(task);
    });
  });
  shedule_.clear();
  shedule_.resize(maxTask);
}

void OperatingSystem::pasteInDeque(Task* task)
{
  size_t i = 0;
  while (i != finalShedule_.size() && task->priority_ > finalShedule_[i]->priority_) {
    i++;
  }
  finalShedule_.insert(finalShedule_.begin() + i, task);
}

void OperatingSystem::showPosixPlained() const
{
  std::for_each(std::cbegin(shedule_), std::cend(shedule_), [](const auto & vector) {
    log("________________________________________________________________");
    std::for_each(std::cbegin(vector), std::cend(vector), [](Task* task)
    {
      log(task->name_ + ' ');
    });
  });
}

