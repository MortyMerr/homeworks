#include "utils.hpp"
#include "OperatingSystem.hpp"
#include <algorithm>
#include <iostream>

void workFunction1(std::vector< Task* >& tasks)
{
  log("working in WorkFunction1");
  for (size_t i = 0; i < tasks.size(); i++) {
    switch (tasks[i]->run()) {
    case 0:
      delete tasks[i];
      tasks.erase(tasks.begin() + i);
      break;
    case 1:
      OperatingSystem::Instance().push(tasks[i]);
      break;
    case 2:
      delete tasks[i];
      OperatingSystem::Instance().updateShedule();
      break;
    }
  }
}

void workFunction2(std::vector< Task* >& tasks)
{
  log("working in WorkFunction2");
  events[0] = true;
  log("turned on signal 0");
}

void workFunction3(std::vector< Task* >& tasks)
{
  log("working in WorkFunction3");
  events[2] = true;
  log("turned on signal 2");
}

double globalTime = 0;

Task::missionFunction func1 = workFunction1,
func2 = workFunction2,
func3 = workFunction3;

Resource* res1 = new Resource("Resource 1"),
*res2 = new Resource("Resource 2"),
*res3 = new Resource("Resource 3");

