#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <fstream>
#include "Task.hpp"

#define maxTask 16
#define maxPrior 16
//extern Task* task1, task2, task4, task5, task6;
extern Task::missionFunction func1, func2, func3;
extern Resource * res1, *res2, *res3;
extern double globalTime;
extern void activateLog();
extern void log(const std::string & string);

#endif // !UTILS_HPP
