#ifndef OPERATING_SYSTEM_HPP
#define OPERATING_SYSTEM_HPP

#include <vector>
#include <deque>
#include "utils.hpp"
#include "Task.hpp"

class OperatingSystem
{
public:
  OperatingSystem(const OperatingSystem &) = delete;
  OperatingSystem(OperatingSystem &&) = delete;
  OperatingSystem & operator =(const OperatingSystem &) = delete;
  OperatingSystem & operator =(OperatingSystem &&) = delete;

  static OperatingSystem& Instance();
  void push(Task* task);
  void start();
  void pasteInDeque(Task* task);
  void updateShedule();
  void showPosixPlained() const;
private:
  OperatingSystem();
  ~OperatingSystem();
  std::vector < std::vector< Task* > > shedule_;
  std::deque <Task*> finalShedule_;
};

#endif