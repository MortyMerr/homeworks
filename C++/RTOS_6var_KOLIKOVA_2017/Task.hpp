#ifndef TASK_HPP
#define TASK_HPP

#include <vector>
#include "resource.hpp"
#include "Events.hpp"

class Task
{
public:
  using missionFunction = void(&)(std::vector< Task* >&);

  Task(double born, 
    double execTime, 
    double period, 
    const std::initializer_list< Resource* >& resources, 
    missionFunction mission, 
    const std::string& name, 
    const std::initializer_list<Task *>&& taskToCall,
    int needSignal,
    size_t priority = 0);
  ~Task();
  int run();
public:
  std::string name_;
  missionFunction mission_;
  std::vector< Resource* > resources_;
  std::vector< Task* > taskToCall_;
  double born_, execTime_, period_, startedTime;
  size_t priority_;
  int needSighnal_;
  std::string getDescription() const;
};

#endif // ! TASK_HPP