#include "Rectangle.hpp"
#include <stdexcept>

Rectangle::Rectangle(double width, double height, const point_t& center) : 
  figure_{ width, height, center }
{
  if (width < 0)
  {
    throw std::invalid_argument("invalid width");
  } 
  if (height < 0)
  {
    throw std::invalid_argument("invalid height");
  }
}

double Rectangle::getArea() const
{
  return figure_.width * figure_.height;
}

rectangle_t Rectangle::getFrameRect() const
{
  return figure_;
}

std::ostream& operator <<(std::ostream & os, const Rectangle& target)
{
  return os << target.figure_;
}

void Rectangle::move(const point_t& target)
{
  figure_.pos = target;
}

void Rectangle::move(double dx, double dy)
{
  figure_.pos.x += dx;
  figure_.pos.y += dy;
}

void Rectangle::scale(double k)
{
  if (k < 0)
  {
    throw std::invalid_argument("k < 0");
  }
  figure_.height *= k;
  figure_.width *= k;
}
