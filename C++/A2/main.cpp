#include "Circle.hpp"
#include "Rectangle.hpp"
#include "Triangle.hpp"
#include <stdexcept>
#define INVALID_ARGUMENT_ERROR 1
int main()
{
  try
  {
    Circle circle(1, { 2, 3 });
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return INVALID_ARGUMENT_ERROR;
  }
  /*try
  {
    Rectangle rectangle(-1, -1, { 0, 0 });
    std::cout << rectangle.getArea();
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return INVALID_ARGUMENT_ERROR;
  }*/
  try
  {
    Triangle triangle({ { 0, 0 },{ 1, 1 },{ 100, 100 } });
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return INVALID_ARGUMENT_ERROR;
  }
  return 0;
}
