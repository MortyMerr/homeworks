#include "CompositeShape.hpp"
#include <algorithm>
#include <iostream>
#define startSize 4

CompositeShape::CompositeShape() :
  shapes_(startSize),
  updatedFrame_(false)
{}

void CompositeShape::push(Shape* shape)
{
  updatedFrame_ = false;
  shapes_.push_back(shape);
}

std::ostream& operator <<(std::ostream & os, const CompositeShape& right)
{
  os << "CompsiteShape\n";
  for (size_t i = 0; i < right.shapes_.size(); i++)
  {
    os << right.shapes_[i] << '\n';
  }
  return os;
}

auto CompositeShape::findXMinMax() const
{
  return std::minmax_element(shapes_.begin(), shapes_.end(), [](const Shape* l, const Shape* r)
  {
    return l->getFrameRect().pos.x + l->getFrameRect().width / 2 
             < r->getFrameRect().pos.x + + r->getFrameRect().width / 2;
  });
}

auto CompositeShape::findYMinMax() const
{
  return std::minmax_element(std::begin(shapes_), std::end(shapes_), [](const Shape* l, const Shape* r)
  {
    return l->getFrameRect().pos.y + l->getFrameRect().height / 2
           < r->getFrameRect().pos.y + r->getFrameRect().height / 2;
  });
}

void CompositeShape::findFrame() const
{
  const auto xMinMax = findXMinMax();
  const auto yMinMax = findYMinMax();
  const double width = (*(xMinMax.second))->getFrameRect().pos.x
    + 0.5 * (*(xMinMax.second))->getFrameRect().width
    - (*(xMinMax.first))->getFrameRect().pos.x
    + 0.5 * (*(xMinMax.first))->getFrameRect().width;
  const double height = (*(yMinMax.second))->getFrameRect().pos.y
    + 0.5 * (*(yMinMax.second))->getFrameRect().height
    - (*(yMinMax.first))->getFrameRect().pos.y
    + 0.5 * (*(yMinMax.first))->getFrameRect().height;
  updatedFrame_ = true;
  frame_ = { width, height,
  { width / 2 + (*(xMinMax.first))->getFrameRect().pos.x
    - 0.5 * (*(xMinMax.first))->getFrameRect().width,
    height / 2 + (*(xMinMax.first))->getFrameRect().pos.y
    - 0.5 * (*(xMinMax.first))->getFrameRect().width }};
}

rectangle_t CompositeShape::getFrameRect() const
{
  if (!updatedFrame_)
  {
    findFrame();
  }
  return frame_;
}

double CompositeShape::getArea() const
{
  double area = 0;
  std::for_each(shapes_.begin(), shapes_.end(), [&area](const Shape* shape) mutable
  {
    area += shape->getArea();
  });
  //TODO accumulate
  return area;
}

void CompositeShape::move(const point_t& target)
{
  if (!updatedFrame_)
  {
    findFrame();
  }
  std::for_each(shapes_.begin(), shapes_.end(), [&target, this](Shape* shape) mutable
  {
    shape->move(target.x - frame_.pos.x,
                target.y - frame_.pos.y);
  });
  updatedFrame_ = false;
}

void CompositeShape::move(double dx, double dy)
{
  std::for_each(shapes_.begin(), shapes_.end(), [dx, dy](Shape* shape)
  {
    shape->move(dx, dy);
  });
  updatedFrame_ = false;
}

void CompositeShape::scale(double k)
{
  std::for_each(shapes_.begin(), shapes_.end(), [k](Shape* shape)
  {
    shape->scale(k);
  });
  updatedFrame_ = false;
}

void CompositeShape::print() const
{
  std::for_each(shapes_.begin(), shapes_.end(), [](const Shape* shape)
  {
    shape->print();
  });
}
