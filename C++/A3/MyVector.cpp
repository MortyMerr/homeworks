//#define _SCL_SECURE_NO_WARNINGS
#include "MyVector.hpp"
#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <iostream>


template < typename T >
MyVector< T >::MyVector(size_t capacity) :
  size_(0),
  capacity_(capacity),
  inf_(new T[capacity])
{
}

template < typename T >
MyVector< T >::MyVector(const std::vector< T > & vector): MyVector< T >(vector.size())
{
  size_ = vector.size();
  std::copy(vector.begin(), vector.end(), this->begin());
}

template < typename T >
MyVector< T >::~MyVector()
{
  for (size_t i = 0; i < size_; i++)
  {
    delete inf_[i];
  }
  delete[] inf_;
}

template < typename T >
void MyVector< T >::push_back(T shape)
{
  if (size_ == capacity_)
  {
    resize();
  }
  inf_[size_++] = shape;
}

template < typename T >
T & MyVector<T>::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }
  return inf_[index];
}

template < typename T >
size_t MyVector< T >::size() const
{
  return size_;
}

template < typename T >
void MyVector< T >::resize()
{
  T* copy = inf_;
  capacity_ *= 2;
  try
  {
    inf_ = new T[capacity_];
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    for (size_t i = 0; i < size_; i++)
    {
      delete copy[i];
    }
    delete[] copy;
  }
  std::copy(copy, copy + size_, inf_);
  /*std::copy(copy, copy + size_, stdext::make_checked_array_iterator<Shape**>(inf_, size_));*/
  //TODO
  //TOASK I don understand warning from this function. I used checked_iterator, but nothing has chacnged
}

template < typename T >
MyIterator< T > MyVector< T >::begin() const
{
  return MyIterator< T >(inf_);
}

template < typename T >
MyIterator< T > MyVector< T >::end() const
{
  return MyIterator< T >(inf_ + size_);
}
template class MyVector< Shape* >;
