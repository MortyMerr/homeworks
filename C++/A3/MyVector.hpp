#ifndef MY_VECTOR_HPP
#define MY_VECTOR_HPP

#include "MyIterator.hpp"
#include "Shape.hpp"
#include <vector>

template <typename T>
class MyVector final
{
public:
  using value_type = T;
  using this_type = MyVector < T >;

  MyVector(size_t capacity);
  MyVector(const std::vector< T > & vector);
  MyVector(const this_type &) = delete;
  MyVector(this_type &&) = delete;
  ~MyVector();

  this_type & operator =(const this_type &) = delete;
  this_type & operator =(this_type &&) = delete;
  value_type & operator [](size_t index) const;

  size_t size() const;
  void push_back(value_type shape);
  void resize();
  MyIterator < T > begin() const;
  MyIterator < T > end() const;
private:
  size_t size_, capacity_;
  value_type * inf_;
};

typedef MyVector<Shape*> shapeVector;

#endif
