#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP

#include "Shape.hpp"
#include <iterator>

template < typename T >
class MyIterator : public std::iterator<std::input_iterator_tag, T>
{
public:
  using value_type = T;
  using this_type = MyIterator < T >;

  MyIterator() = default;
  MyIterator(value_type* p);
  MyIterator(const this_type &it);
  bool operator !=(const this_type & other) const;
  bool operator ==(const this_type& other) const;

  value_type& operator*() const;
  this_type& operator++();
private:
  value_type* p;
};

typedef MyIterator<Shape*> shapeIterator;
#endif // !MY_ITERATOR_HPP
