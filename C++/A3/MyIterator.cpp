#include "MyIterator.hpp"

template<typename T>
MyIterator< T >::MyIterator(T *p) :
    p(p)
{
}

template<typename T>
MyIterator< T >::MyIterator(const MyIterator< T > & it) :
    p(it.p)
{
}

template< typename T >
bool MyIterator< T >::operator !=(const MyIterator< T > & other) const
{
    return p != other.p;
}

template< typename T >
bool MyIterator< T >::operator ==(const MyIterator< T > & other) const
{
    return p == other.p;
}

template< typename T >
T& MyIterator< T >::operator*() const
{
    return *p;
}

template< typename T >
MyIterator< T > & MyIterator< T >::operator ++()
{
    ++p;
    return *this;
}

template class MyIterator< Shape* >;

