#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "MyVector.hpp"
#include "Shape.hpp"

class CompositeShape final : public Shape
{
public:
  CompositeShape();
  friend std::ostream & operator <<(std::ostream & os, const CompositeShape& right);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& target) override;
  void move(double dx, double dy) override;
  void scale(double k) override;
  void print() const override;
  void push(Shape* shape);
public:
  auto findXMinMax() const;
  auto findYMinMax() const;
  void findFrame() const;
  MyVector< Shape* > shapes_;
  mutable rectangle_t frame_;
  mutable bool updatedFrame_;
};

#endif
