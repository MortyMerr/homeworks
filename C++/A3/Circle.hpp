#ifndef CIRCLE_H
#define CIRCLE_H

#include "Shape.hpp"
#include <iostream>

class Circle final : public Shape
{
public:
  Circle(double r, const point_t& center);
  friend std::ostream & operator <<(std::ostream & os, const Circle& right);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& target) override;
  void move(double dx, double dy) override;
  void scale(double k) override;
  void print() const override;
private:
  double r_;
  point_t center_;
};

#endif
