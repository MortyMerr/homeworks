#define _USE_MATH_DEFINES
#include <cmath>
#include "Circle.hpp"
#include <stdexcept>

Circle::Circle(double r, const point_t& center) :
  r_(r),
  center_(center)
{
  if (r < 0)
  {
    throw std::invalid_argument("negative radius");
  }
}

double Circle::getArea() const
{
  return pow(r_, 2) * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return { r_ * 2, r_ * 2, center_ };
}

std::ostream& operator <<(std::ostream & os, const Circle& right)
{
  return os << "Circle center: " << right.center_
    << " r " << right.r_;
}

void Circle::move(const point_t& target)
{
  center_ = target;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::scale(double k) 
{
  if (k < 0)
  {
    throw std::invalid_argument("k < 0");
  }
  r_ *= k;
}

void Circle::print() const
{
  std::cout << *this;
}