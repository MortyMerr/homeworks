#define BOOST_TEST_MAIN
#define _USE_MATH_DEFINES
#include <cmath>
#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "base-types.hpp"
#include "CompositeShape.hpp"
#include "MyVector.hpp"
#include <vector>
#include <boost/test/included/unit_test.hpp>
#define accurancy 1e-12

BOOST_AUTO_TEST_SUITE(CircleSuite)

BOOST_AUTO_TEST_CASE(circleArea)
{
  const Circle circle(3, { 0, 0 });
  BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), M_PI * 3 * 3, accurancy);
}

BOOST_AUTO_TEST_CASE(circleMoveDxDy)
{
  Circle circle(3, { 0, 0 });
  const rectangle_t frame = circle.getFrameRect();
  circle.move(2, 5);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(circleMoveTo)
{
  Circle circle(3, { 0, 0 });
  const rectangle_t frame = circle.getFrameRect();
  circle.move({2, 5});
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(circleFrameRect)
{
  const Circle circle(3, { 0, 0 });
  const rectangle_t frame = circle.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(6, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(6, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(circleInvalidArgument)
{
  BOOST_CHECK_THROW(Circle(-1, { 0, 0 }), std::invalid_argument);
  BOOST_CHECK_THROW(Circle(1, { 0, 0 }).scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleScale)
{
  Circle circle(3, { 0, 0 });
  const double area = circle.getArea(),
    k = 0.5;
  circle.scale(k);
  BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), area * k * k, accurancy);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleSuite)

BOOST_AUTO_TEST_CASE(rectangleArea)
{
  const Rectangle rectangle(3, 2, { 0, 0 });
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 3 * 2, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleMoveDxDy)
{
  Rectangle rectangle(3, 2, { 0, 0 });
  const rectangle_t frame = rectangle.getFrameRect();
  rectangle.move(1, 0);
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleMoveTo)
{
  Rectangle rectangle(3, 2, { 0, 0 });
  const rectangle_t frame = rectangle.getFrameRect();
  rectangle.move({1, 123.3});
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleFrameRect)
{
  Rectangle rectangle(3, 2, { 0, 0 });
  const rectangle_t frame = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(3, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(2, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidArgument)
{
  BOOST_CHECK_THROW(Rectangle(-1, 1, { 0, 0 }), std::invalid_argument);
  BOOST_CHECK_THROW(Rectangle(1, -1, { 0, 0 }), std::invalid_argument);
  BOOST_CHECK_THROW(Rectangle(1, 1, { 0, 0 }).scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleScale)
{
  Rectangle rectangle(3, 2, { 0, 0 });
  const double area = rectangle.getArea(),
    k = 0.5;
  rectangle.scale(k);
  BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), area * k * k, accurancy);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleSuite)

BOOST_AUTO_TEST_CASE(triangleArea)
{
  const Triangle triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } });
  //std::numeric_limits<double>::epsilon();
  BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), 0.5, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleMoveDxDy)
{
  Triangle triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } });
  const rectangle_t frame = triangle.getFrameRect();
  triangle.move(-123.123, 432.123);
  BOOST_CHECK_CLOSE_FRACTION(triangle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(triangle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleMoveTo)
{
  Triangle triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } });
  const rectangle_t frame = triangle.getFrameRect();
  triangle.move({-123.123, 432.123});
  BOOST_CHECK_CLOSE_FRACTION(triangle.getFrameRect().width, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(triangle.getFrameRect().height, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleFrameRect)
{
  const Triangle triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } });
  const rectangle_t frame = triangle.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(1, frame.width, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(1, frame.height, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleScale)
{
  Rectangle triangle(3, 2, { 0, 0 });
  const double area = triangle.getArea(),
    k = 0.5;
  triangle.scale(k);
  BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), area * k * k, accurancy);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(MyVectorSuite)

BOOST_AUTO_TEST_CASE(MyVectorShapePush)
{
  MyVector< Shape* > vector(1);
  std::vector< Shape* > tmp;
  for(size_t i = 0; i < 3; i++)
  {
    Circle* circle = new Circle(3, {1, 8});
    Rectangle* rectangle = new Rectangle(3, 1, {-3, -7});
    Triangle* triangle = new Triangle({{1, 3}, {4, 3}, {3, 7}});
    tmp.push_back(rectangle);
    tmp.push_back(circle);
    tmp.push_back(triangle);
    vector.push_back(rectangle);
    vector.push_back(circle);
    vector.push_back(triangle);
  }
  BOOST_CHECK_EQUAL_COLLECTIONS(tmp.begin(), tmp.end(), vector.begin(), vector.end());
  /*std::for_each(tmp.begin(), tmp.end(), [](Shape* shape)
  {
    delete shape;
  });*/
}

BOOST_AUTO_TEST_CASE(MyVectorShapeContainerConstructor)
{
  std::vector< Shape* > tmp;
  for(size_t i = 0; i < 1000; i++)
  {
    tmp.push_back(new Circle(3, {1, 8}));
    tmp.push_back(new Rectangle(3, 1, {-3, -7}));
    tmp.push_back(new Triangle({{1, 3}, {4, 3}, {3, 7}}));
  }
  const MyVector< Shape* > vector(tmp);
  BOOST_CHECK_EQUAL_COLLECTIONS(tmp.begin(), tmp.end(), vector.begin(), vector.end());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeSuite)

BOOST_AUTO_TEST_CASE(CompositeShapeArea)
{
  CompositeShape composite;
  composite.push(new Circle(3, {1, 8}));
  composite.push(new Rectangle(3, 1, {-3, -7}));
  composite.push(new Triangle({{0, 0}, {0, 1}, {1, 0}}));
  BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), 3 * 3 * M_PI + 3 * 1 + 0.5, accurancy);
}

BOOST_AUTO_TEST_CASE(CompositeGetFrameRect)
{
  CompositeShape composite;
  composite.push(new Circle(3, {1, 0}));
  composite.push(new Rectangle(3, 1, {-3, -7}));
  composite.push(new Triangle({{0, 0}, {0, 1}, {1, 0}}));
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.x, -0.25, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.y, -3.25, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().width, 8.5, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().height, 10.5, accurancy);
}

BOOST_AUTO_TEST_CASE(CompositeMoveDxDy)
{
  CompositeShape composite;
  composite.push(new Circle(3, {1, 0}));
  composite.push(new Rectangle(3, 1, {-3, -7}));
  composite.push(new Triangle({{0, 0}, {0, 1}, {1, 0}}));
  composite.move(3, 2);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.x, 2.75, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.y, -1.25, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().width, 8.5, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().height, 10.5, accurancy);
  //TOASK should i check Shapes(triangle, circle, rectangle) coordinate?
}

BOOST_AUTO_TEST_CASE(CompositeMoveTo)
{
  CompositeShape composite;
  composite.push(new Circle(3, {1, 0}));
  composite.push(new Rectangle(3, 1, {-3, -7}));
  composite.push(new Triangle({{0, 0}, {0, 1}, {1, 0}}));
  composite.move({3, 2});
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.x, 3, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.y, 2, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().width, 8.5, accurancy);
  BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().height, 10.5, accurancy);
}

BOOST_AUTO_TEST_CASE(CompositeScale)
{
  CompositeShape composite;
  composite.push(new Circle(3, {1, 0}));
  composite.push(new Rectangle(3, 1, {-3, -7}));
  composite.push(new Triangle({{0, 0}, {0, 1}, {1, 0}}));
  const double area = composite.getArea();
  composite.scale(0.3);
  BOOST_CHECK_CLOSE_FRACTION(area * 0.3 * 0.3, composite.getArea(), accurancy);
}

BOOST_AUTO_TEST_SUITE_END()
