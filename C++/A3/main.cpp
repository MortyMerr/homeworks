#include "Circle.hpp"
#include "Rectangle.hpp"
#include "Triangle.hpp"
#include "MyVector.hpp"
#include "CompositeShape.hpp"
#define INVALID_ARGUMENT_ERROR 1
int main()
{
  /*MyVector myVector(4);
  for (int i = 0; i < 100; i++) {
    myVector.push_back(new Rectangle(1, 1, { 1, 1 }));
    std::cout << myVector[i]->getFrameRect();
    myVector[i]->print();
  }*/
  CompositeShape comp;
  comp.push(new Rectangle(1, 2, { 1, 1 }));
  comp.push(new Circle(1, { 10, 10 }));
  std::cout << comp.getFrameRect();
  std::cout << comp.getArea();
  return 0;
}
