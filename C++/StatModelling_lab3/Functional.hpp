#ifndef FUNCTIONAL_HPP 
#define FUNCTIONAL_HPP
#define sizeVec 10000
#define PI 3.14
#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>

extern double dRand(double _max, double _min);
extern double normal_1(const double m, const double sigma);
extern double normal_2(const double m, const double sigma);
extern double exponential(const double b);
extern double xi(const size_t N);
extern double student(const size_t N);

#endif