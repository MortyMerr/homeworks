#include "Functional.hpp"

double dRand(double _max, double _min)
{
  return _min + double(rand()) / RAND_MAX * (_max - _min);
}

double normal_1(const double m, const double sigma)
{
  return sqrt(-2 * log(dRand(0, 1))) * sin(2 * PI + dRand(0, 1)) 
    + sqrt(-2 * log(dRand(0, 1))) * cos(2 * PI + dRand(0, 1))
    - 1;
}
double normal_2(const double m, const double sigma)
{
  double sum = 0;
  for (size_t i = 0; i < 12; i++)
  {
    sum += dRand(0, 1);
  }
  sum -= 6;
  return sum;
 //For true norm distribution return m + sum * sigma;
}

double exponential(const double b)
{
  return -b * log(dRand(0, 1));
}

double xi(const size_t N)
{
  double sum = 0;
  for (size_t i = 0; i < N; i++)
  {
    sum += pow(normal_2(1, 0), 2);
  }
  return sum;
}

double student(const size_t N)
{
  return normal_2(1, 0) / sqrt(xi(N) / N);
}
