#ifndef FUNCTIONAL_HPP
#define FUNCTIONAL_HPP
#include <vector>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <numeric>

#define sizeVec 10000

/// <summary>
/// uniform distrubution
/// </summary>
/// <param name="_max">The maximum.</param>
/// <param name="_min">The minimum.</param>
/// <returns></returns>
extern double dRand(double _max, double _min);
/// <summary>
/// normal(gaussian) distribution
/// </summary>
/// <param name="Mx">The mx.</param>
/// <param name="Sigma">The sigma.</param>
/// <returns></returns>
extern double rnorm(double const Mx, const double Sigma);

/// <summary>
/// Logarifm distribution
/// not good sd, but itaksoidet
/// </summary>
/// <param name="q">The q.</param>
/// <returns></returns>
extern size_t logarifm(const double q);

/// <summary>
/// puasson distribution �1
/// </summary>
/// <param name="mu">The mu.</param>
/// <returns></returns>
extern size_t puas_1(const int mu);

/// <summary>
/// puasson distribution �2
/// </summary>
/// <param name="mu">The mu.</param>
/// <returns></returns>
extern size_t puas_2(const int mu);

/*geometric*/
/// <summary>
/// geometric alg �2 
/// </summary>
/// <param name="p">The p.</param>
/// <returns></returns>
extern size_t geom_2(const double p);

/// <summary>
/// geometric recurrent formula alg �1
/// </summary>
/// <param name="p">The p.</param>
/// <returns></returns>
extern size_t geom_1(const double p);

/// <summary>
/// geometric recurrent formula modified alg �3
/// don't work, itaksoidet
/// </summary>
/// <param name="p">The p.</param>
/// <returns></returns>
extern size_t geom_3(const double p);

/// <summary>
/// binominal with normal aproximation
/// </summary>
/// <param name="N">The n.</param>
/// <param name="p">The p.</param>
/// <returns></returns>
extern double bin_2(const size_t N, const double p);

/// <summary>
/// Cummulative algorithm
/// </summary>
/// <param name="N">n</param>
/// <param name="p">p</param>
/// <returns>r</returns>
extern size_t bin_1(const size_t N, const double p);

#endif 