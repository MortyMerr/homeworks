#include "Functional.hpp"

int main()
{
  std::vector <double> mainVec(sizeVec);
  for (double& i : mainVec)
  {
    //i = P(1, 100);
    //i = bin(10, 0.5);
    //i = geom(0.5);
    //i = puas(10);
    i = bin_2(10, 0.5);
  }
  double m = std::accumulate(mainVec.begin(), mainVec.end(), 0.0) / sizeVec;
  double d = 0;
  for (double& i : mainVec)
  {
    d += pow((i - m), 2);
  }
  d /= sizeVec;
  std::cout << std::setw(5) << m << " " << std::setw(5) << d;
  system("pause>>null");
  return 0;
}