#include "Functional.hpp"

double dRand(double _max, double _min)
{
  return _min + double(rand()) / RAND_MAX * (_max - _min);
}

double rnorm(double const Mx, const double Sigma)
{
  double a = 0, b = 0, r = 0;
  do {
    a = 2 * dRand(0, 1) - 1;
    b = 2 * dRand(0, 1) - 1;
    r = a * a + b * b;
  } while (r >= 1);
  double Sq = sqrt(-2 * log(r) / r);
  return Mx + Sigma * a * Sq;
}

size_t logarifm(const double q)
{
  size_t m = 1;
  double P = -1 / log(1 - q) * (1 - q) / m, M = dRand(0, 1);
  while (M >= 0)
  {
    M -= P;
    P *= (1 - q) / m;
    m++;
  }
  return --m;
}

size_t puas_1(const int mu)
{
  double P = exp(-mu), M = dRand(0, 1);
  size_t m = 0;
  while (M >= 0)
  {
    m++;
    M -= P;
    P = P / m * mu;
  }
  return --m;
}

size_t puas_2(const int mu)
{
  size_t counter = 0;
  double proizv = 1;
  while (proizv >= exp(-mu))
  {
    proizv *= dRand(0, 1);
    counter++;
  }
  return --counter;
}

size_t geom_2(const double p)
{
  int counter = 0;
  while (dRand(0, 1) < p)
  {
    counter++;
  }
  return counter + 1;
}

size_t geom_1(const double p)
{
  double P = p, M = dRand(0, 1);
  size_t m = 0;
  while (M >= 0)
  {
    M -= P;
    P *= (1 - p);
    m++;
  }
  return m;
}

size_t geom_3(const double p)
{
  double P = p, M = dRand(0, 1);
  double u = 0;
  size_t m = 0;
  while (M >= 0)
  {
    M -= P;
    P *= (1 - p);
    u += P;
    m++;
  }
  return (int)(log(u) / log(1 - p)) + 1;
}

double bin_2(const size_t N, const double p)
{
  return rnorm(N * p, sqrt(N * p * (1 - p))) + 0.5;
}

size_t bin_1(const size_t N, const double p)
{
  double P = pow(1 - p, N), M = dRand(0, 1);
  size_t m = 0;
  while (M >= 0)
  {
    M -= P;
    P *= (N - m) * p / (m + 1) / (1 - p);
    m++;
  }
  return --m;
}
