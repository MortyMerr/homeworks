#ifndef ELEMENT_HPP
#define ELEMENT_HPP
#include "utils.hpp"
#include <cmath>

class Element
{
public:
  Element(const double lambda);
  void generateT();
  double getT() const;
  int check(const double time) const;
  void clear();
private:
  double lambda_, time_;
};

#endif