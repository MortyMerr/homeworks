#include "Stucture.hpp"


void Stucture::generateT(const size_t type)
{
  for (Element& el : struct_[type])
  {
    el.generateT();
  }
  middle.resize(3);
  for (size_t i = 0; i < struct_.size(); i++)
  {
    for (size_t j = 0; j < struct_[i].size(); j++)
    {
      middle[i] += struct_[i][j].getT();
    }
    middle[i] = (double)middle[i] / struct_[i].size();
  }
}

Stucture::Stucture(const double l1, const double l2, const double l3)
{
  stat_.resize(3);
  std::vector <Element> tmp;
  for (size_t i = 0; i < firstSize; i++)
  {
    tmp.push_back(Element(l1));
  }
  struct_.push_back(tmp);
  std::vector <Element> tmp2;
  for (size_t i = 0; i < secondSize; i++)
  {
    tmp2.push_back(Element(l2));
  }
  struct_.push_back(tmp2);
  std::vector <Element> tmp3;
  for (size_t i = 0; i < thirdSize; i++)
  {
    tmp3.push_back(Element(l3));
  }
  struct_.push_back(tmp3);
}

void Stucture::replace(const size_t type)
{
  Element* min = &struct_[type][0];
  for (Element& el : struct_[type])
  {
    if (el.getT() < min->getT())
    {
      min = &el;
    }
  }
  min->generateT();
}

int Stucture::checkFirst(const double time) const
{
  return (struct_[0][0].check(time) && struct_[0][1].check(time)) || struct_[0][2].check(time);
}

int Stucture::checkSecond(const double time) const
{
  return (struct_[1][0].check(time) && struct_[1][1].check(time));
}

int Stucture::checkThird(const double time) const
{
  return (struct_[2][0].check(time) && struct_[2][1].check(time))
    || (struct_[2][2].check(time) && struct_[2][3].check(time))
    || (struct_[2][4].check(time) && struct_[2][5].check(time));
}

int Stucture::check(const double time)
{
  const int checked[3] = { checkFirst(time), checkSecond(time), checkThird(time) };
  int flag = 1;
  for (size_t i = 0; i < 3; i++)
  {
    if (!checked[i])
    {
      stat_[i]++;
      flag = 0;
    }
  }
  if (!flag)
  {
    return 0;
  }
  return 1;
}

size_t Stucture::getWorst()
{
   return (std::max_element(stat_.begin(), stat_.end()) - stat_.begin());
}

void Stucture::clear()
{
  for (std::vector<Element>& group : struct_)
  {
    for (Element& el : group)
    {
      el.clear();
    }
  }
  middle.clear();
}

void Stucture::clearStat()
{
  stat_.clear();
}

void Stucture::printStat()
{
  std::copy(stat_.begin(), stat_.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout << '\n';
}