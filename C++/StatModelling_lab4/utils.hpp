#ifndef UTILS_HPP
#define UTILS_HPP
#include <numeric>
#include <vector>
///constants
extern size_t N, d, firstSize, secondSize, thirdSize, T, m;
extern double P0, l1, l2, l3, e, Ta, alpha;
extern std::vector <size_t> L;
//_________________
extern double dRand(const double _max, const double _min);

#endif