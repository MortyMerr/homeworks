#include "Element.hpp"

Element::Element(const double lambda):lambda_(lambda), time_(0)
{}

void Element::generateT()
{
  time_ += -log(dRand(0, 1)) / lambda_;
}

double Element::getT() const
{
  return time_;
}

int Element::check(const double time) const
{
  return (time_ > time);
}

void Element::clear()
{
  time_ = 0;
}