#include "utils.hpp"

//constants
size_t firstSize = 3, secondSize = 2, thirdSize = 6,
N = 0, 
d = 0,
T = 8760,
m = 3;
double l1 = 40 * 1e-6, l2 = 10 * 1e-6, l3 = 80 * 1e-6,
P0 = 0.999,
e = 0.001,
aplha = 0.99,
Ta = 2.326348; //qnorm(alpha) � Rstudio

std::vector <size_t> L = { 1, 1, 1 };
//_______________

double dRand(const double _max, const double _min)
{
  return _min + double(rand()) / RAND_MAX * (_max - _min);
}