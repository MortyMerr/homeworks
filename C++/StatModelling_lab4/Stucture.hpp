#ifndef STRUCTURE_HPP
#define STUCTURE_HPP
#include "utils.hpp"
#include "Element.hpp"
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

class Stucture
{
public:
  Stucture(const double l1, const double l2, const double l3);
  Stucture() = default;
  void generateT(const size_t type);
  void replace(const size_t type);
  int check(const double time);
  void clear();
  void clearStat();
  size_t getWorst();
  void printStat();
public:
  std::vector <std::vector <Element> > struct_;
  std::vector <size_t> stat_, middle;
  int checkFirst(const double time) const;
  int checkSecond(const double time) const;
  int checkThird(const double time) const;

};

#endif // !STRUCTURE_HPP