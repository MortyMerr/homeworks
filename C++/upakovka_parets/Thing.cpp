#include "Thing.hpp"

Thing::Thing() : weight_(1 + rand() % paramLimit), //� ������ � ���� ������������ ���, ����� � �����
amount_(1 + rand() % paramLimit),
num_(counter++),
u_(0)
{
  for (size_t i = 0; i < paramMax; i++)
  {
    params_.push_back(1 + rand() % paramLimit);//� ��� �� ������������ ���������
  }
}

std::ostream& operator <<(std::ostream & os, const Thing& tmp)
{
  os << std::setw(4) << tmp.num_;
  for (const int i : tmp.params_)
  {
    os << std::setw(4) << i;
  }
  return os << std::setw(4) << tmp.weight_ << std::setw(4) << tmp.amount_;//��� ���������� ����� � ���� ���� ���������� ����
}



bool operator ==(const Thing& l, const Thing& r) {
  int count = 0;//������ ������� ����� ��� ����� ������
  for (int i = 0; i < paramLimit; i++)
  {
    if (l.params_[i] > r.params_[i])//���� �������� ���� ����� ������ ��� �������� ������ - ���������� �������
    {
      count++;
    }
    else if (l.params_[i] < r.params_[i])
      count--;//���� ������ - ��������
  }
  return (count == 0);//���� ���������� ��������� ������� ������ � ���������� ���������� ������� ������ ����� ����� �����, ��
  //���� ������������� ���� �����
}

bool operator <(const Thing& l, const Thing& r)
{
  if (l == r)
  {
    return false;
  }
  return !(l > r);
}

bool operator >(const Thing& l, const Thing& r)
{
  //����� ������� ����� ������������ ����� ����� �������� �� ��������
  int count = 0;
  for (int i = 0; i < paramLimit; i++)
  {
    if (l.params_[i] > r.params_[i])
    {
      count++;
    }
    else if (l.params_[i] < r.params_[i])
    {
      count--;
    }
  }//��������� ���������� ����������, ������� ������������� ��������� >
  return (count > 0);//���� ����� ��� � �  == ������ ������ >
}
