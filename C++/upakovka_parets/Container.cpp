#include "Container.hpp"



Container::Container(): num_(containerNumber++),//� ������ ��������� ����� �����, ��������� ��� � �����(������ �������������
freeAmount_(amountMax),
freeWeight_(weightMax),
count_(0)
{
}

bool Container::push(const Thing& tmp)
{
  if ((tmp.weight_ < freeWeight_) && (tmp.amount_ < freeAmount_))//���� ���� ������, ��
  {
    loadedThings_.push_back(tmp);//������ ����
    freeAmount_ -= tmp.amount_;//��������� ��������� ��� � �����
    freeWeight_ -= tmp.weight_;
    count_++;
    return true;//���������� �����
  }
  return false;//����� �������
}