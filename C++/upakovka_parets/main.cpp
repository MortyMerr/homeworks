#include <fstream>
#include <ctime>
#include <algorithm>
//��� ���������� ���������
#include "Thing.hpp"//���� ������ � ���� ����
#include "Container.hpp"//���� ������ � ���� ����

size_t O1(const std::vector<Container> tmp)//�������� �1
{
  size_t count = 0;//������� ���������� ����� �� ���� �����������
  for (const Container& i : tmp)
  {
    count += i.count_;
  }
  return count;
}

size_t O2(const std::vector <Container>& tmp, std::vector <Thing> things)//�������� �2
{

  size_t count = O1(tmp);//�������� ���������� ����� �� ���� �����������.
  for (const Container& i : tmp)//������ ������ �� ������ things �� ����, ������� �� �������� � ����������
  {
    for (const Thing& j : i.loadedThings_)
    {
      if (std::find(things.begin(), things.end(), j) != things.end())//���� ���� � ����������
      {
        things.erase(std::find(things.begin(), things.end(), j));//������� �� ������
      }
    }
  }
  for (const Container& i : tmp)//������ ���������� ������ ���� � ����������� � ����������� �������
    //���� ������� ������� ����� ����, ��� �� U ������ - �������� �� count �������(�� ���� ��� ���� �� ����� ���� �������������)
  {
    for (const Thing& j : i.loadedThings_)
    {
      for (const Thing& k : things)
      {
        if (j.u_ == k.u_)
        {
          count--;
          break;
        }
      }
    }
  }
  return count;
}

int main()
{
  srand(unsigned(time(NULL)));
  double marks[M][M];//������� ������ �������� ��������
  std::vector <Thing> things(20);
  std::ofstream fout("input.txt");
  for (const Thing& tmp : things)
  {
    fout << std::setw(4) << tmp << '\n';
  }//������ ��������������� ����
  
  fout << "\n\n������� ������\n\n";
  fout << "     | ";
  for (size_t i = 0; i < M; i++)
  {
    fout << std::setw(4) << i << "|";//������ ��������
  }

  for (size_t i = 0; i < M; i++)
  {
    fout << '\n' << std::setw(4) << i << " | ";//������ �����
    for (size_t j = 0; j < M; j++)
    {
      if (i == j)//��������� ������� ������
      {
        marks[i][j] = 0.5;
      }
      else if (things[i] == things[j])
      {
        marks[i][j] = 0.5;
      }
      else if (things[i] > things[j])
      {
        marks[i][j] = 1;
      }
      else
      {
        marks[i][j] = 0;
      }
      fout << std::setw(5) << marks[i][j];
    }
  }

  std::sort(things.begin(), things.end());//��������� � ����������� � �������� �������� ����������
  fout << "\n\n������������\n\n";
  std::vector <std::vector <Thing>> parets;
  std::vector <Thing> sloi;//������� �������� ��������� ����
  for (size_t i = 0; i < things.size(); i++)
  {
    for (double j : marks[things[i].num_])
    {
      things[i].u_ += j;
    }
    sloi.push_back(things[i]);
    fout << things[i] << std::setw(6) << things[i].u_ << '\n';
    if ((i != 0) && !(things[i] == things[i - 1]))
    {
      parets.push_back(sloi);
      sloi.clear();
      fout << '\n';
    }
  }

  //��������
  std::vector <std::pair <double, double>> coord;//������ ��� �������� o1, o2
  std::sort(things.begin(), things.end(), [](const Thing& l, const Thing& r) { return (l.u_ > r.u_);});//�������������
  //�� �������� U
  std::vector<Container> containers(containerNum);//������ �����������
  for (size_t i = 0; i < things.size(); i++)//����� � ���������. �� ������������� ���� �� �������
    //U � ������ ����� �� ����������� �� �������. ���� � ����� ���������� ��� ����� - ������� ���������. ���� ��� ���� 
    //����� �� ������� ����� - ������� ��������� ���� � ��� �����.
    //�� ��������� ���� �� ������ ��� �� � ������ ����, � �� ������  (��������, ��� ������� ��������� ������ �����)
    //������� ��� ���� ��������� ���������� o1 o2
  {
    for (size_t j = i; j < things.size(); j++)
    {
      for (Container& container : containers)//������� ��� ����������
      {
        if (container.push(things[j]))//���� ������� ��������
        {
          break;//�����
        }
      }
    }
    coord.push_back(std::make_pair<double, double>(O1(containers), O2(containers, things)));//��������� ����������
    for (Container& clear : containers)//�������� ����������
    {
      clear.loadedThings_.clear();
      clear.freeWeight_ = weightMax;
      clear.freeAmount_ = amountMax;
      clear.count_ = 0;
    }
  }
  fout.close();
  return 0;
}