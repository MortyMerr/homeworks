  #ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.hpp"
#include <vector>
#include <iostream>

class Triangle final : public Shape
{
public:
  Triangle(const std::vector <point_t>& vertices);
  friend std::ostream & operator <<(std::ostream & os, const Triangle& right);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& target) override;
  void move(double dx, double dy) override;
private:
  point_t center_;
  double side(const point_t& a, const point_t& b) const;
  std::vector <point_t> vertices_;
  void updateCenter();
};

#endif
