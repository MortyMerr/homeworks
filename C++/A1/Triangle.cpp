#include "Triangle.hpp"
#include <iterator>
#include <algorithm>
#include <stdexcept> 

Triangle::Triangle(const std::vector <point_t>& vertices): 
  vertices_(vertices)
{
}

std::ostream& operator <<(std::ostream& os, const Triangle& right)
{
  os << "Triangle: ";
  std::copy(right.vertices_.begin(), right.vertices_.end(), std::ostream_iterator<point_t>(os, "\n"));
  return os;
}

double Triangle::getArea() const
{
  const double AB = side(vertices_[0], vertices_[1]),
    BC = side(vertices_[1], vertices_[2]),
    AC = side(vertices_[0], vertices_[2]),
    p = (AB + BC + AC) / 2;
  return sqrt(p * (p - AB) * (p - AC) * (p - BC));
}

rectangle_t Triangle::getFrameRect() const
{
  const auto xMinMax = std::minmax_element(vertices_.begin(), vertices_.end(), [](const point_t& left, const point_t& right)
  {
    return left.x < right.x;
  });
  const auto yMinMax = std::minmax_element(vertices_.begin(), vertices_.end(), [](const point_t& left, const point_t& right)
  {
    return left.y < right.x;
  });
  const double width = xMinMax.second->x - xMinMax.first->x;
  const double height = yMinMax.second->y - yMinMax.first->y;
  return rectangle_t{ width, height, point_t { width / 2 + xMinMax.first->x, height / 2 + xMinMax.first->y} };
}

void Triangle::move(const point_t& target)
{
  updateCenter();
  move(target.x - center_.x, target.y - center_.y);
}

void Triangle::move(double dx, double dy)
{
  const point_t offset{ dx, dy };
  std::for_each(vertices_.begin(), vertices_.end(), [offset](point_t& tmp)
  {
    tmp.x += offset.x;
    tmp.y += offset.y;
  });
}

double Triangle::side(const point_t& a, const point_t& b) const
{
  return sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2));
}

void Triangle::updateCenter()
{
  center_ = { 0, 0 };
  for (const point_t& p : vertices_)
  {
    center_.x += p.x;
    center_.y += p.y;
  }
  center_.x /= 3;
  center_.y /= 3;
}