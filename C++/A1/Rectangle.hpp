#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.hpp"
#include <iostream>

class Rectangle final : public Shape
{
public:
  Rectangle(double width, double height, const point_t& center);
  friend std::ostream & operator <<(std::ostream & os, const Rectangle& right);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& target) override;
  void move(double dx, double dy) override;
private:
  rectangle_t figure_;
};

#endif
