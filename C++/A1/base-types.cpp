#include "base-types.hpp"

std::ostream & operator <<(std::ostream& os, const point_t& right)
{
  return os << '('
    << right.x << ", "
    << right.y
    << ')';
}

std::ostream & operator <<(std::ostream& os, const rectangle_t& right)
{
  return os << right.pos
    << " width " << right.width
    << " height " << right.height;
}
